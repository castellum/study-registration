import datetime
from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_bootstrap5',
    'bootstrap_colors',
    'study_registration.utils',
    'study_registration.users',
    'study_registration.studies',
    'study_registration.publications',
    'study_registration.datasets',
    'study_registration.budgets',
    'study_registration.deployments',
    'study_registration.biosamples',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'study_registration.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            BASE_DIR / 'templates',
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'study_registration.context_processors.get_settings',
            ],
        },
    },
]

WSGI_APPLICATION = 'study_registration.wsgi.application'

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTH_USER_MODEL = 'users.User'

LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'
LOGIN_URL = '/login/'

# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_TZ = True

LOCALE_PATHS = [
    BASE_DIR / 'locale',
]


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATICFILES_DIRS = [
    BASE_DIR / 'static',
]

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'study_registration.utils.finders.NpmFinder',
]

STORAGES = {
    'default': {
        'BACKEND': 'django.core.files.storage.FileSystemStorage',
    },
    'staticfiles': {
        'BACKEND': 'django.contrib.staticfiles.storage.ManifestStaticFilesStorage',
    },
}

STATIC_URL = 'static/'

STATIC_ROOT = BASE_DIR.parent / 'static'

NPM_PATH = BASE_DIR.parent / 'node_modules'
NPM_FILES = {
    Path('bootstrap', 'dist', 'css', 'bootstrap.min.css'),
    Path('bootstrap', 'dist', 'css', 'bootstrap.min.css.map'),
    Path('bootstrap', 'dist', 'js', 'bootstrap.min.js'),
    Path('bootstrap', 'dist', 'js', 'bootstrap.min.js.map'),
    Path('@popperjs', 'core', 'dist', 'umd', 'popper-lite.min.js'),
    Path('@popperjs', 'core', 'dist', 'umd', 'popper-lite.min.js.map'),
    Path('@fortawesome', 'fontawesome-free', 'css', 'all.min.css'),
    Path('@fortawesome', 'fontawesome-free', 'webfonts', 'fa-regular-400.woff2'),
    Path('@fortawesome', 'fontawesome-free', 'webfonts', 'fa-regular-400.ttf'),
    Path('@fortawesome', 'fontawesome-free', 'webfonts', 'fa-brands-400.woff2'),
    Path('@fortawesome', 'fontawesome-free', 'webfonts', 'fa-brands-400.ttf'),
    Path('@fortawesome', 'fontawesome-free', 'webfonts', 'fa-solid-900.woff2'),
    Path('@fortawesome', 'fontawesome-free', 'webfonts', 'fa-solid-900.ttf'),
    Path('@fortawesome', 'fontawesome-free', 'webfonts', 'fa-v4compatibility.woff2'),
    Path('@fortawesome', 'fontawesome-free', 'webfonts', 'fa-v4compatibility.ttf'),
    Path('select', 'tags.js'),
    Path('select', 'select.js'),
    Path('select', 'utils.js'),
    Path('select', 'values.js'),
    Path('select', 'select.css'),
}

BOOTSTRAP5 = {
    'set_placeholder': False,
    'required_css_class': 'form-required',
}

BOOTSTRAP_THEME_COLORS = None

LOCATION_PREFIXES = [
    'FB-ABC/',
    'FB-ARC/',
    'FB-CHM/',
    'FB-EuB/',
    'FB-LIP/',
    'LMG-EnvNeuro/',
    'MPRG-Affect/',
    'MPRG-Biosocial/',
    'MPRG-isearch/',
    'MPRG-Neurocode/',
    'MPRG-NSC/',
    'MPRG-REaD/',
]

BUDGETS_APPROVAL_THRESHOLD = 1500

DEPLOYMENTS_GITLAB_BASE_URL = None
DEPLOYMENTS_DURATION_MAX = datetime.timedelta(days=183)

# See https://docs.gitlab.com/ee/api/rest/#authentication
# Example: {'Authorization': 'Bearer MY_TOKEN'}
DEPLOYMENTS_GITLAB_HEADERS = {}

RDM_NOTIFICATIONS_TO = []
PUBLICATION_NOTIFICATIONS_TO = []
BIOSAMPLES_NOTIFICATIONS_TO = []

STORAGE_BASE_URL = None
STORAGE_HEADERS = {}

ETHICS_NEXT_STEPS = []
CREATE_TICKET_URL = None
