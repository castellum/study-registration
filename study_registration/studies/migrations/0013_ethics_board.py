from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("studies", "0012_study_imported_from"),
    ]

    operations = [
        migrations.CreateModel(
            name="EthicsBoard",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "name",
                    models.CharField(max_length=254, unique=True, verbose_name="Name"),
                ),
            ],
            options={
                "ordering": ["name"],
            },
        ),
        migrations.AddField(
            model_name="study",
            name="ethics_board",
            field=models.ForeignKey(
                blank=True,
                help_text="Please remember to save the application and approval to the study folder.",
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="studies.ethicsboard",
                verbose_name="Ethics board",
            ),
        ),
    ]
