from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('users', '0003_replace_approve_flag_by_permission'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='office',
            field=models.CharField(blank=True, max_length=64),
        ),
        migrations.AddField(
            model_name='user',
            name='phone',
            field=models.CharField(blank=True, max_length=64),
        ),
    ]
