from django.urls import path

from .views import BioSampleSetCreateView
from .views import BioSampleSetDeleteView
from .views import BioSampleSetUpdateView

app_name = 'biosamples'
urlpatterns = [
    path('new/', BioSampleSetCreateView.as_view(), name='create'),
    path('<int:pk>/update/', BioSampleSetUpdateView.as_view(), name='update'),
    path('<int:pk>/delete/', BioSampleSetDeleteView.as_view(), name='delete'),
]
