from django.conf import settings
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView
from django.views.generic import DeleteView
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import UpdateView

from study_registration.studies.mixins import StudyAspectMixin
from study_registration.users.models import User
from study_registration.utils.mail import send_mail
from study_registration.utils.mail import send_study_mail

from .forms import BudgetForm
from .models import Budget


class BudgetCreateView(StudyAspectMixin, CreateView):
    model = Budget
    form_class = BudgetForm
    ethics_required = True

    def send_notification(self):
        to = (
            User.objects
            .with_perm('budgets.approve', include_superusers=False)
            .exclude(email=None)
            .exclude(is_active=False)
            .values_list('email', flat=True)
        )

        user = self.request.user.get_full_name()
        threshold = settings.BUDGETS_APPROVAL_THRESHOLD
        link = self.request.build_absolute_uri(
            reverse(
                'budgets:approve',
                args=[self.study.pk, self.study.short_title, self.object.pk],
            )
        )

        subject = settings.EMAIL_SUBJECT_PREFIX + 'Budget approval requested'
        body = (
            f'{user} registered a study budget over {threshold}\xa0€. '
            f'They are asking for your approval:\n\n{link}'
        )

        return send_mail(subject, body, None, to)

    def form_valid(self, form):
        form.instance.study = self.study
        self.object = form.save()
        self.send_notification()
        return redirect(self.get_success_url())


class BudgetUpdateView(StudyAspectMixin, UpdateView):
    model = Budget
    form_class = BudgetForm


class BudgetDeleteView(StudyAspectMixin, DeleteView):
    model = Budget


class ApproverRequiredMixin(PermissionRequiredMixin):
    permission_required = ['budgets.approve']

    def get_queryset(self):
        return (
            super().get_queryset()
            .filter(expected__gte=settings.BUDGETS_APPROVAL_THRESHOLD)
            .order_by('-pk')
        )


class BudgetListView(ApproverRequiredMixin, ListView):
    model = Budget
    paginate_by = 20
    ordering = ['-pk']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filters'] = [
            ('', _('All')),
            ('approved', _('Approved')),
            ('not-approved', _('Not approved')),
        ]
        return context

    def get_queryset(self):
        qs = super().get_queryset().select_related(
            'study__contact_person',
            'approved_by',
        )
        if self.request.GET.get('filter') == 'approved':
            return qs.exclude(approved_by=None)
        elif self.request.GET.get('filter') == 'not-approved':
            return qs.filter(approved_by=None)
        else:
            return qs


class BudgetApproveView(ApproverRequiredMixin, StudyAspectMixin, DetailView):
    model = Budget
    template_name_suffix = '_approve'
    owner_required = False

    def send_notification(self):
        user = self.request.user.get_full_name()
        study = self.study.title
        if self.object.approved_by:
            body = f'A budget for your study "{study}" got approved by {user}.'
        else:
            body = f'A budget for Your study "{study}" got declined by {user}.'

        return send_study_mail('Study approval changed', body, self.study, self.request)

    def post(self, request, **kwargs):
        self.object = self.get_object()

        if self.request.POST.get('approved'):
            self.object.approved_by = request.user
        else:
            self.object.approved_by = None
        self.object.save()

        self.send_notification()
        return redirect(request.path)
