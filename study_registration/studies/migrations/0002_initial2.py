from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('studies', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='study',
            name='contact_person',
            field=models.ForeignKey(
                help_text='The contact person must be reachable. Before the current contact person leaves the institute, they must pass on the study to someone else.',
                limit_choices_to={'is_active': True},
                on_delete=django.db.models.deletion.PROTECT,
                related_name='+',
                to=settings.AUTH_USER_MODEL,
                verbose_name='Contact person',
            ),
        ),
        migrations.AddField(
            model_name='study',
            name='department',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                to='studies.department',
                verbose_name='Department',
            ),
        ),
        migrations.AddField(
            model_name='study',
            name='owners',
            field=models.ManyToManyField(
                to=settings.AUTH_USER_MODEL,
                verbose_name='Users who can edit this study profile',
            ),
        ),
        migrations.AddField(
            model_name='keyword',
            name='study',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to='studies.study'
            ),
        ),
        migrations.AlterUniqueTogether(
            name='keyword',
            unique_together={('study', 'name')},
        ),
    ]
