from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import gettext_lazy as _

from .models import User


class UserAdmin(BaseUserAdmin):
    list_filter = [*BaseUserAdmin.list_filter, 'default_department']
    fieldsets = (
        (None, {'fields': ['username', 'password']}),
        (_('Personal info'), {
            'fields': ['first_name', 'last_name', 'email', 'phone', 'office']
        }),
        (_('Defaults'), {
            'fields': ['default_department'],
        }),
        (_('Permissions'), {
            'fields': [
                'is_active',
                'is_staff',
                'is_superuser',
                'groups',
                'user_permissions',
            ],
        }),
        (_('Important dates'), {'fields': ['last_login', 'date_joined']}),
    )


admin.site.register(User, UserAdmin)
