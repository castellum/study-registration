import traceback
from time import sleep

from django.core.management.base import BaseCommand

from study_registration.studies import storage
from study_registration.studies.models import Study


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-w', '--watch', action='store_true')

    def do_sync(self):
        for study in Study.objects.filter(
            ldap_group_managed=True, ldap_group_synced=False
        ):
            self.stdout.write(f'syncing {study}')
            storage.sync_group(study)
            study.ldap_group_synced = True
            study.save()

    def handle(self, *args, **options):
        if options['watch']:
            while True:
                try:
                    self.do_sync()
                except Exception:
                    traceback.print_exc()
                sleep(5)
        else:
            self.do_sync()
