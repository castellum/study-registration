from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('studies', '0009_rm_study_url_fields'),
    ]

    operations = [
        migrations.AddField(
            model_name='study',
            name='storage_location',
            field=models.CharField(
                blank=True,
                max_length=255,
                null=True,
                unique=True,
                verbose_name='Storage location',
            ),
        ),
        migrations.AlterField(
            model_name='study',
            name='owners',
            field=models.ManyToManyField(
                to=settings.AUTH_USER_MODEL,
                verbose_name='Users who can edit this study profile and access the private folder',
            ),
        ),
    ]
