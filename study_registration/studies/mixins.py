from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404

from .models import Study


class StudyAspectMixin(LoginRequiredMixin):
    owner_required = True
    ethics_required = False

    def dispatch(self, request, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        if request.user.is_superuser or not self.owner_required:
            self.study = get_object_or_404(
                Study, pk=kwargs['study_pk'], short_title=kwargs['study_short']
            )
        else:
            self.study = get_object_or_404(
                Study,
                pk=kwargs['study_pk'],
                short_title=kwargs['study_short'],
                owners=request.user,
            )
        if self.ethics_required and not self.study.ethics_confirmed:
            return self.handle_no_permission()
        return super().dispatch(request, **kwargs)

    def get_queryset(self):
        return super().get_queryset().filter(study=self.study)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['study'] = self.study
        return context

    def get_success_url(self):
        return self.study.get_absolute_url()
