from .default import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'CHANGEME'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# disable cache busting (necessary for testing)
STORAGES = {
    'default': {
        'BACKEND': 'django.core.files.storage.FileSystemStorage',
    },
    'staticfiles': {
        'BACKEND': 'django.contrib.staticfiles.storage.StaticFilesStorage',
    },
}

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR.parent / 'db.sqlite3',
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

try:
    import debug_toolbar

    INTERNAL_IPS = ['127.0.0.1', 'localhost']
    INSTALLED_APPS += ['debug_toolbar']
    MIDDLEWARE = ['debug_toolbar.middleware.DebugToolbarMiddleware'] + MIDDLEWARE
    DEBUG_TOOLBAR_CONFIG = {'SHOW_COLLAPSED': True}
except ImportError:
    pass

CREATE_TICKET_URL = 'https://tickets.example.com'
RDM_NOTIFICATIONS_TO = ['rdm@example.com']
PUBLICATION_NOTIFICATIONS_TO = ['publications@example.com']
BIOSAMPLES_NOTIFICATIONS_TO = ['bio@example.com']
