from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('studies', '0002_initial2'),
    ]

    operations = [
        migrations.CreateModel(
            name='Budget',
            fields=[
                (
                    'id',
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                (
                    'expected',
                    models.DecimalField(
                        decimal_places=2,
                        max_digits=12,
                        verbose_name='Expected cost (€)',
                    ),
                ),
                (
                    'actual',
                    models.DecimalField(
                        blank=True,
                        decimal_places=2,
                        max_digits=12,
                        null=True,
                        verbose_name='Actual cost (€)',
                    ),
                ),
                (
                    'approved_by',
                    models.ForeignKey(
                        blank=True,
                        limit_choices_to={'can_approve_budget': True},
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        related_name='+',
                        to=settings.AUTH_USER_MODEL,
                        verbose_name='Approved by',
                    ),
                ),
                (
                    'study',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to='studies.study',
                        verbose_name='Study',
                    ),
                ),
            ],
        ),
    ]
