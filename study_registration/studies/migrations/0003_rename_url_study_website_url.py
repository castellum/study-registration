from django.db import migrations
from django.db import models


class Migration(migrations.Migration):

    dependencies = [
        ('studies', '0002_initial2'),
    ]

    operations = [
        migrations.RenameField(
            model_name='study',
            old_name='url',
            new_name='website_url',
        ),
        migrations.AlterField(
            model_name='study',
            name='website_url',
            field=models.URLField(blank=True, verbose_name='Website'),
        ),
        migrations.AddField(
            model_name='study',
            name='code_url',
            field=models.URLField(blank=True, verbose_name='Public code repository'),
        ),
        migrations.AddField(
            model_name='study',
            name='data_url',
            field=models.URLField(blank=True, verbose_name='Public data repository'),
        ),
        migrations.AddField(
            model_name='study',
            name='prereg_url',
            field=models.URLField(blank=True, verbose_name='Preregistration'),
        ),
    ]
