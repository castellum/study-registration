from django.db import models
from django.utils.translation import gettext_lazy as _

from study_registration.studies.models import Study
from study_registration.utils.fields import DateField


class BioSampleSet(models.Model):
    study = models.ForeignKey(Study, verbose_name=_('Study'), on_delete=models.CASCADE)
    sample_type = models.CharField(_('Type of samples'), max_length=64, help_text=_(
        'If you have different types of samples, please register them separately.')
    )
    count = models.PositiveIntegerField(_('Number of expected total samples'))
    temperature = models.SmallIntegerField(_('Freezer Temperature'), choices=[
        (1, _('-80°C')),
        (2, _('-20°C')),
        (3, _('-4°C')),
        (4, _('room temperature')),
    ])
    processing = models.TextField(_('Further processing at MPIB'), blank=True)
    analyses = models.TextField(_('Planned analyses'), blank=True)
    laboratories = models.TextField(_('Analyzing laborator(y/ies)'), blank=True)
    keep_analyzed = models.BooleanField(
        _('We will store samples that come back from labs')
    )
    keep_backup = models.BooleanField(_('We will store backup samples'))
    date_of_disposal = DateField(_('Date of disposal'))
    notes = models.TextField(_('Notes'), blank=True)

    def __str__(self):
        return self.sample_type
