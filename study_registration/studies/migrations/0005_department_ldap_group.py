from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('studies', '0004_optional_features'),
    ]

    operations = [
        migrations.AddField(
            model_name='department',
            name='ldap_group',
            field=models.CharField(
                blank=True, max_length=254, verbose_name='LDAP group'
            ),
        ),
    ]
