from django.db import models
from django.utils.translation import gettext_lazy as _

from study_registration.studies.models import Study
from study_registration.utils.fields import DateField


class DataSet(models.Model):
    STRUCTURE_BIDS = 'bids'
    STRUCTURE_RPS = 'rps'
    STRUCTURE_OTHER = 'other'

    REUSE_NONE = 1
    REUSE_INTERNAL = 2
    REUSE_RESEARCH = 3
    REUSE_GENERAL = 4

    SOURCE_NEW = 1
    SOURCE_REUSE_INTERNAL = 2
    SOURCE_REUSE_EXTERNAL = 3

    study = models.ForeignKey(Study, on_delete=models.CASCADE)

    source = models.SmallIntegerField(_('Data source'), choices=[
        (SOURCE_NEW, _('Newly collected')),
        (SOURCE_REUSE_INTERNAL, _('Reused from within the MPIB')),
        (SOURCE_REUSE_EXTERNAL, _('Reused from an external data provider')),
    ], default=SOURCE_NEW)

    reuse_study = models.ForeignKey(
        Study,
        verbose_name=_('Study that the data you are reusing originated from'),
        on_delete=models.PROTECT,
        related_name='+',
        blank=True,
        null=True,
    )
    reuse_source = models.CharField(
        _('Original data source'),
        help_text=_('e.g., DOI, URL, research institution, contact person'),
        max_length=254,
        blank=True,
    )

    collection_start = DateField(_('Start of data collection'), blank=True, null=True)
    collection_end = DateField(_('End of data collection'), blank=True, null=True)
    structure = models.CharField(
        _('Data structure'),
        max_length=32,
        blank=True,
        choices=[
            (STRUCTURE_BIDS, _('BIDS')),
            (STRUCTURE_RPS, _('R Package Structure')),
            (STRUCTURE_OTHER, _('Other')),
        ],
    )
    structure_documentation = models.TextField(
        _('Description of structure'),
        help_text=_(
            'If you cannot use one of these standards to structure your data, '
            'please describe how your data and metadata will be structured '
            '(e.g. folder and file naming conventions). This description '
            'should enable others to navigate your data folder.'
        ),
        blank=True,
    )

    data_reuse = models.SmallIntegerField(
        _('Data reuse'),
        help_text=_(
            'Which data reuse scenarios have participants agreed to in '
            'the study consent?'
        ),
        blank=True,
        null=True,
        choices=[
            (REUSE_NONE, _('1. Generally not possible')),
            (REUSE_INTERNAL, _('2. Possible within the institute')),
            (REUSE_RESEARCH, _('3. Possible for research purposes')),
            (REUSE_GENERAL, _('4. Generally possible')),
        ],
    )
    consent_location = models.CharField(
        _('Consent location'),
        help_text=_(
            'Internal path to the signed informed consent forms, '
            'e.g. "FB-ARC/Projects/FaceHouse/Ethics/Consent" or '
            '"Cupboard in Dillenburger Str., Room 306."'
        ),
        max_length=254,
        blank=True,
    )
