"""Convert SMB locations.

Windows uses UNCs, all other platforms use URLs.

-   Use URLs internally.
-   Use ``smb_url_or_unc`` in templates to pick the right display based on
    User-Agent
"""


def url(s):
    if s.startswith('smb://'):
        return s
    elif s.startswith('\\\\'):
        return 'smb:' + s.replace('\\', '/')
    else:
        raise TypeError(s)


def unc(s):
    if s.startswith('\\\\'):
        return s
    elif s.startswith('smb://'):
        return s.removeprefix('smb:').replace('/', '\\')
    else:
        raise TypeError(s)


def url_or_unc(request, s):
    if 'Windows' in request.headers.get('User-Agent', ''):
        return unc(s)
    else:
        return url(s)
