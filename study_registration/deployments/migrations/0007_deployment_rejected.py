from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("deployments", "0006_deployment_legacy_branch"),
    ]

    operations = [
        migrations.AddField(
            model_name="deployment",
            name="rejected",
            field=models.BooleanField(default=False, verbose_name="Rejected"),
        ),
    ]
