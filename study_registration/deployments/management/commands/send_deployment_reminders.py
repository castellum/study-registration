import datetime

from django.core.management.base import BaseCommand
from django.urls import reverse

from study_registration.deployments.models import Deployment
from study_registration.utils.mail import send_study_mail


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('base_url')

    def handle(self, *args, **options):
        today = datetime.date.today()
        next_week = today + datetime.timedelta(days=7)

        for deployment in (
            Deployment.objects
            .filter(
                rejected=False,
                end_reminded=False,
                enddate__gte=today,
                enddate__lte=next_week,
            )
            .exclude(revisions=None)
            .select_related('study', 'target')
        ):
            url = options['base_url'] + reverse('deployments:redeploy', args=[
                deployment.study.pk, deployment.study.short_title, deployment.pk
            ])
            success = send_study_mail(
                'Deployment is about to end',
                (
                    f'Your deployment on {deployment.target} in study '
                    f'{deployment.study} will end on {deployment.enddate}. '
                    f'You can extend the deployment period on {url}.'
                ),
                deployment.study,
                options['base_url'],
            )
            if success:
                deployment.end_reminded = True
                deployment.save()
