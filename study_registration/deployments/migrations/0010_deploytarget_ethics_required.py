from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("deployments", "0009_deployment_end_reminded"),
    ]

    operations = [
        migrations.AddField(
            model_name="deploytarget",
            name="ethics_required",
            field=models.BooleanField(default=True, verbose_name="Ethics required"),
        ),
    ]
