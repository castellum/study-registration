import datetime

from django import forms
from django.conf import settings
from django.core.validators import MaxValueValidator
from django.utils.translation import gettext_lazy as _

from .models import Deployment


class DeploymentForm(forms.ModelForm):
    class Meta:
        model = Deployment
        fields = [
            'git_repo',
            'git_ref',
            'git_file',
            'gitlab_id',
            'commit_sha',
            'target',
            'startdate',
            'enddate',
        ]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields['startdate'].initial = datetime.date.today()
        self.fields['enddate'].validators.append(
            MaxValueValidator(limit_value=(
                datetime.date.today() + settings.DEPLOYMENTS_DURATION_MAX
            ))
        )

    def clean(self):
        cleaned_data = super().clean()
        startdate = cleaned_data.get('startdate')
        enddate = cleaned_data.get('enddate')

        if startdate and enddate and enddate <= startdate:
            self.add_error(
                'enddate',
                _('The deployment cannot end before it has even started')
            )
