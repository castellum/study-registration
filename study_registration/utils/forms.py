from django import forms


class TagField(forms.MultipleChoiceField):
    def widget_attrs(self, widget):
        attrs = super().widget_attrs(widget)
        attrs['data-tags'] = ''
        attrs['data-tags-input-class'] = 'form-control'
        attrs['data-tags-value-class'] = 'badge text-bg-secondary badge-delete'
        attrs['data-tags-separators'] = 'Enter , ;'
        return attrs

    def valid_value(self, value):
        return True


class DateInput(forms.DateInput):
    def __init__(self, attrs=None):
        defaults = {'type': 'date'}
        if attrs:
            defaults.update(attrs)
        super().__init__(format='%Y-%m-%d', attrs=defaults)


class DateField(forms.DateField):
    widget = DateInput


class DatalistWidget(forms.TextInput):
    template_name = 'utils/widgets/datalist.html'

    def __init__(self, *args, datalist=[], **kwargs):
        self.datalist = datalist
        super().__init__(*args, **kwargs)

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context['widget']['attrs']['list'] = context['widget']['attrs']['id'] + '_list'
        context['datalist'] = self.datalist
        return context
