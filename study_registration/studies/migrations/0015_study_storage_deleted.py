from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("studies", "0014_study_storage_managed"),
    ]

    operations = [
        migrations.AddField(
            model_name="study",
            name="storage_deleted",
            field=models.DateField(
                blank=True,
                help_text="The data is no longer available, neither from the storage location nor the archive.",
                null=True,
                verbose_name="Storage deleted",
            ),
        ),
    ]
