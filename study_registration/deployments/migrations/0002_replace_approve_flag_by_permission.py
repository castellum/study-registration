from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('deployments', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='deployment',
            options={'permissions': [('approve', 'Can approve')]},
        ),
    ]
