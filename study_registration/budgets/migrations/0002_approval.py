from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('budgets', '0001_initial2'),
    ]

    operations = [
        migrations.AddField(
            model_name='budget',
            name='backgroundmotivation',
            field=models.TextField(
                blank=True, verbose_name='Theoretical Background and motivation'
            ),
        ),
        migrations.AddField(
            model_name='budget',
            name='deliverable',
            field=models.TextField(
                blank=True, verbose_name='Anticipated Deliverables for the Study'
            ),
        ),
        migrations.AddField(
            model_name='budget',
            name='design',
            field=models.TextField(blank=True, verbose_name='Design of the study'),
        ),
        migrations.AddField(
            model_name='budget',
            name='researchquestion',
            field=models.TextField(
                blank=True,
                verbose_name='Research question and implications for the literature',
            ),
        ),
        migrations.AddField(
            model_name='budget',
            name='timeline',
            field=models.TextField(blank=True, verbose_name='Time Line'),
        ),
    ]
