from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError

from .models import Budget


class BudgetForm(forms.ModelForm):
    class Meta:
        model = Budget
        fields = [
            'expected',
            'actual',
            'motivation',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.threshold = settings.BUDGETS_APPROVAL_THRESHOLD

    def clean(self):
        cleaned_data = super().clean()

        if cleaned_data.get('expected', 0) >= self.threshold:
            motivation_field = self.fields['motivation']
            if not cleaned_data.get('motivation'):
                err = ValidationError(
                    motivation_field.error_messages['required'], code='required'
                )
                self.add_error('motivation', err)

        return cleaned_data
