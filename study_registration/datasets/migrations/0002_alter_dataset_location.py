from django.db import migrations, models
import study_registration.datasets.models


class Migration(migrations.Migration):

    dependencies = [
        ('datasets', '0001_initial2'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dataset',
            name='location',
            field=models.CharField(
                help_text='Internal path to the data, e.g. FB-ARC/Projects/FaceHouse/Data.',
                max_length=254,
                unique=True,
                verbose_name='Storage location',
            ),
        ),
    ]
