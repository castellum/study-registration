from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('studies', '0003_rename_url_study_website_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='department',
            name='enable_budgets',
            field=models.BooleanField(default=False, verbose_name='Enable budgets'),
        ),
        migrations.AddField(
            model_name='department',
            name='enable_deployments',
            field=models.BooleanField(default=False, verbose_name='Enable deployments'),
        ),
    ]
