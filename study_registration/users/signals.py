def ldap_set_department(user=None, ldap_user=None, **kwargs):
    from study_registration.studies.models import Department

    values = ldap_user.attrs.get('department', [])

    # if a department is already set, keep it unless it is no longer valid
    if user.default_department and (
        not user.default_department.ldap_department
        or user.default_department.ldap_department in values
    ):
        return

    for department in Department.objects.exclude(ldap_department=''):
        if department.ldap_department in values:
            user.default_department = department
