from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from .models import Publication


class PublicationForm(forms.ModelForm):
    class Meta:
        model = Publication
        fields = ['doi', 'title', 'authors', 'journal', 'year']

    def clean_doi(self):
        value = self.cleaned_data['doi']

        if not value:
            return value

        value = value.removeprefix('doi:')
        value = value.removeprefix('https://doi.org/')

        if not value.startswith('10.'):
            raise ValidationError(_('DOI must start with 10.'))

        return value
