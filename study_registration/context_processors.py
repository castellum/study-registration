from django.conf import settings


def get_settings(request):
    return {
        'BOOTSTRAP_THEME_COLORS': settings.BOOTSTRAP_THEME_COLORS,
    }
