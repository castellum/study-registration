import datetime

from django.core.exceptions import SuspiciousOperation
from django.http import Http404
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import View
from requests import HTTPError

from study_registration.studies.mixins import StudyAspectMixin
from study_registration.utils import smb

from . import gitlab
from .models import Deployment
from .models import DeployTarget


class DeploymentsApiView(View):
    def serialize_deployment(self, deployment):
        return {
            'deployment_identifier': deployment.deployment_id,
            'study_identifier': deployment.legacy_study_id or str(deployment.study),
            'study_url': self.request.build_absolute_uri(
                reverse(
                    'studies:api',
                    args=[deployment.study.pk, deployment.study.short_title],
                )
            ),
            'branch': deployment.generated_branch,
            'repository': deployment.git_repo,
            'archivefolderpath': (
                deployment.study.storage_location
                and smb.unc(deployment.study.storage_location)
            ),
            'archivefolderpath_url': deployment.study.storage_location,
            'start_script': deployment.git_file,
        }

    def get(self, request, **kwargs):
        qs = (
            Deployment.objects
            .exclude(revisions=None)
            .exclude(rejected=True)
            .select_related('study')
        )

        if 'target' in request.GET:
            target = get_object_or_404(DeployTarget, key=request.GET['target'])
            qs = qs.filter(target=target)

        if 'ended_from' in request.GET:
            try:
                ended_since = datetime.date.fromisoformat(request.GET['ended_from'])
            except ValueError:
                return JsonResponse({'error': 'Invalid ended_from'}, status=400)
            qs = qs.filter(enddate__gte=ended_since)

        if 'ended_until' in request.GET:
            try:
                ended_until = datetime.date.fromisoformat(request.GET['ended_until'])
            except ValueError:
                return JsonResponse({'error': 'Invalid ended_until'}, status=400)
            qs = qs.filter(enddate__lte=ended_until)

        if 'ended_from' not in request.GET and 'ended_until' not in request.GET:
            today = datetime.date.today()
            qs = qs.filter(enddate__gte=today)

        return JsonResponse({'deployments': [self.serialize_deployment(d) for d in qs]})


class BaseGitlabApiView(StudyAspectMixin, View):
    def get_param_or_400(self, key):
        if key not in self.request.GET:
            raise SuspiciousOperation
        return self.request.GET[key]

    def is_project_member(self):
        gitlab_id = self.get_param_or_400('gitlab_id')
        try:
            return gitlab.is_project_member(gitlab_id, self.request.user.username)
        except HTTPError as e:
            if e.response.status_code == 404:
                raise Http404
            else:
                raise

    def dispatch(self, request, **kwargs):
        if not request.user.is_superuser and not self.is_project_member():
            return JsonResponse({'error': 'no access to repository'}, status=403)
        return super().dispatch(request, **kwargs)


class GitlabProjectApiView(BaseGitlabApiView):
    def get(self, request, **kwargs):
        gitlab_id = self.get_param_or_400('gitlab_id')

        project = gitlab.get_project(gitlab_id)
        branches = gitlab.get_branches(gitlab_id)

        return JsonResponse({
            'git_repo': project['ssh_url_to_repo'],
            'default_branch': project['default_branch'],
            'branches': [b['name'] for b in branches],
        })


class GitlabCommitsApiView(BaseGitlabApiView):
    def get(self, request, **kwargs):
        gitlab_id = self.get_param_or_400('gitlab_id')
        branch = self.get_param_or_400('branch')

        commits = gitlab.get_commits(gitlab_id, branch)

        return JsonResponse({
            'commits': [{
                'id': c['id'],
                'title': c['title'],
            } for c in commits]
        })


class GitlabFilesApiView(BaseGitlabApiView):
    def get(self, request, **kwargs):
        gitlab_id = self.get_param_or_400('gitlab_id')
        ref = self.get_param_or_400('ref')

        files = gitlab.get_files(gitlab_id, ref)

        return JsonResponse({
            'files': files,
        })
