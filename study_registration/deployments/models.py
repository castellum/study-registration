import datetime

from django.db import models
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _

from study_registration.studies.models import Department
from study_registration.studies.models import Study
from study_registration.utils.fields import DateField


class DeployTarget(models.Model):
    name = models.CharField(_('Name'), max_length=256, unique=True)
    key = models.SlugField(_('key'), max_length=32, unique=True)
    departments = models.ManyToManyField(
        Department,
        verbose_name=_('Departments that can use this target'),
    )
    ethics_required = models.BooleanField(_('Ethics required'), default=True)
    gitlab_deploy_key = models.PositiveIntegerField(
        _('GitLab deploy key'), blank=True, null=True
    )
    url_template = models.CharField(
        _('Target URL template'), max_length=256, blank=True
    )

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class BaseDeployment(models.Model):
    class Meta:
        abstract = True

    created_at = models.DateField(_('Created at'), auto_now_add=True)
    git_ref = models.CharField(_('Branch'), max_length=64, default='main')
    git_file = models.CharField(
        _('Name of the file within the repository that starts your experiment'),
        help_text=_(
            'A desktop shortcut to this file will be created on the '
            'laboratory computers.'
        ),
        max_length=256,
    )
    commit_sha = models.CharField(_('Commit (version)'), max_length=50)
    startdate = DateField(_('Start of deployment'))
    enddate = DateField(
        _('End of deployment'),
        help_text=_(
            'Your files and collected data will automatically be deleted '
            'from the deployment target after that date.'
        ),
    )


class Deployment(BaseDeployment):
    study = models.ForeignKey(Study, on_delete=models.CASCADE, verbose_name=_('Study'))
    legacy_study_id = models.CharField(_('Legacy study ID'), max_length=254, blank=True)
    legacy_branch = models.CharField(_('Legacy branch'), max_length=254, blank=True)

    git_repo = models.CharField(_('Repository URL'), max_length=256)
    gitlab_id = models.IntegerField(
        _('GitLab project ID'),
        help_text=_(
            'The project ID is displayed right below the project title on GitLab.'
        ),
    )
    target = models.ForeignKey(
        DeployTarget, verbose_name=_('Deploy target'), on_delete=models.PROTECT
    )
    targeturl = models.CharField(_('Target URL'), max_length=300, blank=True)
    rejected = models.BooleanField(_('Rejected'), default=False)
    end_reminded = models.BooleanField(default=False)

    class Meta:
        permissions = [
            ('approve', _('Can approve')),
        ]

    def __str__(self):
        return '{} {} {}'.format(
            self.git_repo.split('/')[-1],
            self.git_ref,
            self.git_file,
        )

    @property
    def deployment_id(self):
        return self.legacy_study_id or f'{self.study}-{self.id}'

    @property
    def generated_branch(self):
        return self.legacy_branch or f'deployment-{self.id}'

    def create_revision(self):
        self.revisions.create(
            deployment=self,
            git_ref=self.git_ref,
            git_file=self.git_file,
            commit_sha=self.commit_sha,
            startdate=self.startdate,
            enddate=self.enddate,
        )

    @cached_property
    def deployed(self):
        return self.revisions.exists()

    @property
    def expired(self):
        return self.enddate < datetime.date.today()

    @property
    def status(self):
        if self.rejected:
            return _('rejected')
        elif not self.deployed:
            return _('not running')
        elif self.expired:
            return _('expired')
        else:
            return _('running')

    @property
    def status_color(self):
        if self.rejected:
            return 'danger'
        elif not self.deployed:
            return 'danger'
        elif self.expired:
            return 'danger'
        else:
            return 'success'


class DeploymentRevision(BaseDeployment):
    deployment = models.ForeignKey(
        Deployment, on_delete=models.CASCADE, related_name='revisions'
    )
