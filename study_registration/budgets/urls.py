from django.urls import path

from .views import BudgetApproveView
from .views import BudgetCreateView
from .views import BudgetDeleteView
from .views import BudgetUpdateView

app_name = 'budgets'
urlpatterns = [
    path('new/', BudgetCreateView.as_view(), name='create'),
    path('<int:pk>/update/', BudgetUpdateView.as_view(), name='update'),
    path('<int:pk>/delete/', BudgetDeleteView.as_view(), name='delete'),
    path('<int:pk>/approve/', BudgetApproveView.as_view(), name='approve'),
]
