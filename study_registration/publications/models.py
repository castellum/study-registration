from django.db import models
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _

from study_registration.studies.models import Study


class Publication(models.Model):
    study = models.ForeignKey(Study, verbose_name=_('Study'), on_delete=models.CASCADE)
    doi = models.CharField(_('DOI'), max_length=254, blank=True, null=True, unique=True)
    title = models.CharField(_('Title'), max_length=254)
    authors = models.CharField(_('Authors'), max_length=254)
    journal = models.CharField(_('Journal'), max_length=254, blank=True)
    year = models.IntegerField(
        _('Year'),
        help_text=_(
            'When adding a publication, please remember '
            'to update "to be stored until" for the data folder.'
        ),
    )

    def __str__(self):
        return self.doi or gettext('Publication')

    def doi_url(self):
        return 'https://doi.org/' + self.doi

    def text(self):
        base = f'{self.authors} ({self.year}). {self.title}.'
        if self.journal:
            base += f' {self.journal}.'
        if self.doi:
            base += f' {self.doi_url()}'
        return base
