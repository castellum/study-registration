from django.conf import settings

if settings.DEBUG and not settings.DEPLOYMENTS_GITLAB_BASE_URL:
    from .stub import *  # noqa
else:
    from .real import *  # noqa
