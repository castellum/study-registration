from django.contrib import admin

from .models import Deployment
from .models import DeploymentRevision
from .models import DeployTarget

admin.site.register(DeployTarget)
admin.site.register(Deployment)
admin.site.register(DeploymentRevision)
