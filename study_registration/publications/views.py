from django.conf import settings
from django.views.generic import CreateView
from django.views.generic import DeleteView
from django.views.generic import UpdateView

from study_registration.studies.mixins import StudyAspectMixin
from study_registration.utils.mail import send_study_mail

from .forms import PublicationForm
from .models import Publication


class PublicationCreateView(StudyAspectMixin, CreateView):
    model = Publication
    form_class = PublicationForm

    def send_notification(self):
        text = self.object.text()
        user = self.request.user.get_full_name()
        study = self.study.title
        link = self.request.build_absolute_uri(self.study.get_absolute_url())

        body = (
            f'{user} added the following publication to the study '
            f'"{study}" ({link}):\n\n'
            f'  {text}\n\n'
            'This notification was also sent to the Research Data & Information, '
            'who will add the publication to the MPS publication repository '
            'https://pure.mpg.de.'
        )

        return send_study_mail(
            'New publication',
            body,
            self.study,
            self.request,
            additional_to=settings.PUBLICATION_NOTIFICATIONS_TO,
        )

    def form_valid(self, form):
        form.instance.study = self.study
        response = super().form_valid(form)
        self.send_notification()
        return response


class PublicationUpdateView(StudyAspectMixin, UpdateView):
    model = Publication
    form_class = PublicationForm


class PublicationDeleteView(StudyAspectMixin, DeleteView):
    model = Publication
