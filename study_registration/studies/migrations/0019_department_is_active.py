# Generated by Django 4.2.9 on 2024-01-09 10:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('studies', '0018_study_ethics_confirmed'),
    ]

    operations = [
        migrations.AddField(
            model_name='department',
            name='is_active',
            field=models.BooleanField(default=True, verbose_name='active'),
        ),
    ]
