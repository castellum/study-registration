from django.db import migrations, models
import django.db.models.deletion


def migrate_data(apps, schema_editor):
    Study = apps.get_model('studies', 'Study')
    Link = apps.get_model('studies', 'Link')

    WEBSITE = 1
    PREREG = 2
    DATA = 3
    CODE = 4

    links = []

    for study in Study.objects.all():
        for type, url in [
            (WEBSITE, study.website_url),
            (PREREG, study.prereg_url),
            (DATA, study.data_url),
            (CODE, study.code_url),
        ]:
            if url:
                links.append(Link(study=study, type=type, url=url))

    Link.objects.bulk_create(links)


class Migration(migrations.Migration):

    dependencies = [
        ('studies', '0007_projects'),
    ]

    operations = [
        migrations.CreateModel(
            name='Link',
            fields=[
                (
                    'id',
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                ('url', models.URLField(verbose_name='URL')),
                (
                    'type',
                    models.SmallIntegerField(
                        choices=[
                            (1, 'Website'),
                            (2, 'Preregistration'),
                            (3, 'Public data repository'),
                            (4, 'Public code repository'),
                        ],
                        verbose_name='Type',
                    ),
                ),
                (
                    'study',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to='studies.study'
                    ),
                ),
            ],
            options={
                'ordering': ['type', 'pk'],
            },
        ),
        migrations.RunPython(migrate_data),
    ]
