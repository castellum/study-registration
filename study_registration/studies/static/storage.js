var button = document.querySelector('#fallback');
var progress = document.querySelector('#progress');

button.hidden = true;
progress.parentElement.removeAttribute('hidden');

var width = 0;

setInterval(() => {
    width = 100 - (100 - width) * 0.99;
    progress.setAttribute('width', width);
}, 50);

button.click();
