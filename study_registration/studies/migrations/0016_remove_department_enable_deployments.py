from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("studies", "0015_study_storage_deleted"),
        ("deployments", "0008_deploytarget_departments"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="department",
            name="enable_deployments",
        ),
    ]
