from django.urls import path

from .views import PublicationCreateView
from .views import PublicationDeleteView
from .views import PublicationUpdateView

app_name = 'publications'
urlpatterns = [
    path('new/', PublicationCreateView.as_view(), name='create'),
    path('<int:pk>/update/', PublicationUpdateView.as_view(), name='update'),
    path('<int:pk>/delete/', PublicationDeleteView.as_view(), name='delete'),
]
