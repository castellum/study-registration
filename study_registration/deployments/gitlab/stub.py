def get_project(project_id):
    return {
        'ssh_url_to_repo': 'https://example.com',
        'default_branch': 'main',
    }


def is_project_member(project_id, username):
    return True


def get_branches(project_id):
    return [{'name': 'main'}]


def get_commits(project_id, ref):
    return [{
        'id': 'abcdefg',
        'title': 'example',
    }]


def get_files(project_id, ref):
    return ['example.txt']


def enable_deploy_key(project_id, key_id):
    pass


def create_deploy_branch(project_id, branch, ref):
    pass
