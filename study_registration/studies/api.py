from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.generic import View

from .marcxml import marcxml
from .models import Link
from .models import Study


def serialize_department(department):
    if department:
        return {
            'name': department.name,
        }


def serialize_project(project):
    if project:
        return {
            'title': project.title,
        }


def serialize_user(user):
    if user:
        return {
            'username': user.username,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email,
        }


def serialize_ethiks_board(ethics_board):
    if ethics_board:
        return {
            'name': ethics_board.name,
        }


def serialize_dataset(request, dataset):
    if dataset.source == dataset.SOURCE_REUSE_INTERNAL:
        return {
            'source': dataset.get_source_display(),
            'reuse_study': (
                request.build_absolute_uri(dataset.reuse_study.get_absolute_url())
                if dataset.reuse_study else None,
            ),
        }
    elif dataset.source == dataset.SOURCE_REUSE_EXTERNAL:
        return {
            'source': dataset.get_source_display(),
            'reuse_source': dataset.reuse_source,
        }

    else:
        return {
            'source': dataset.get_source_display(),
            'collection_start': dataset.collection_start,
            'collection_end': dataset.collection_end,
            'structure': dataset.get_structure_display(),
            'structure_documentation': dataset.structure_documentation,
            'data_reuse': dataset.get_data_reuse_display(),
            'consent_location': dataset.consent_location,
        }


def serialize_publication(publication):
    return {
        'doi': publication.doi,
        'title': publication.title,
        'authors': publication.authors,
        'journal': publication.journal,
        'year': publication.year,
    }


def serialize_study(request, study):
    return {
        'id': str(study),
        'url': request.build_absolute_uri(study.get_absolute_url()),
        'created_at': study.created_at,
        'updated_at': study.updated_at,
        'owners': [serialize_user(owner) for owner in study.owners.all()],
        'title': study.title,
        'short_title': study.short_title,
        'aliases': study.aliases,
        'end': study.end,
        'description': study.description,
        'department': serialize_department(study.department),
        'project': serialize_project(study.project),
        'website_urls': [link.url for link in study.link_set.filter(type=Link.WEBSITE)],
        'prereg_urls': [link.url for link in study.link_set.filter(type=Link.PREREG)],
        'data_urls': [link.url for link in study.link_set.filter(type=Link.DATA)],
        'code_urls': [link.url for link in study.link_set.filter(type=Link.CODE)],
        'contact_person': serialize_user(study.contact_person),
        'affiliated_scientists': study.affiliated_scientists,
        'ethics_board': serialize_ethiks_board(study.ethics_board),
        'ethics_confirmed': study.ethics_confirmed,
        'storage_location': study.storage_location,
        'storage_archived': study.storage_archived,
        'storage_archive_name': study.get_storage_archive_name(),
        'storage_deleted': study.storage_deleted,
        'ldap_group_managed': study.ldap_group_managed,
        'keywords': [keyword.name for keyword in study.keyword_set.all()],
        'datasets': [serialize_dataset(request, d) for d in study.dataset_set.all()],
        'publications': [serialize_publication(p) for p in study.publication_set.all()],
    }


class StudyJSONView(View):
    def get(self, request, **kwargs):
        study = get_object_or_404(
            Study, pk=self.kwargs['pk'], short_title=self.kwargs['slug']
        )
        return JsonResponse(serialize_study(request, study))


class StudyMarcXMLView(View):
    def get(self, request, **kwargs):
        study = get_object_or_404(
            Study, pk=self.kwargs['pk'], short_title=self.kwargs['slug']
        )
        return marcxml(request, [study])
