from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("publications", "0001_initial2"),
    ]

    operations = [
        migrations.AlterField(
            model_name="publication",
            name="journal",
            field=models.CharField(blank=True, max_length=254, verbose_name="Journal"),
        ),
    ]
