from django.db import migrations, models
import django.db.models.deletion
import study_registration.utils.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('studies', '0002_initial2'),
    ]

    operations = [
        migrations.CreateModel(
            name='DataSet',
            fields=[
                (
                    'id',
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                (
                    'location',
                    models.CharField(
                        help_text='Internal path to the data, e.g. FB-ARC/Projects/FaceHouse/Data.',
                        max_length=254,
                        unique=True,
                        verbose_name='Storage location',
                    ),
                ),
                (
                    'size',
                    models.IntegerField(
                        help_text='A rough estimate that obviously may need adaptation. Rules of thumb are: structural MR data = xx GB per sequence per participant; functional MR data = xx GB per sequence per participant; EEG data = xx GB per sequence per participant',
                        verbose_name='Expected size (in GB)',
                    ),
                ),
                (
                    'to_be_stored_until_year',
                    models.IntegerField(
                        help_text='Data is typically required to be kept for 10 years after the last publication based on the rules of good scientific practice. After that, it is deleted as required by GDPR. This field may need to be adapted as new articles are published.',
                        verbose_name='To be stored until (year)',
                    ),
                ),
                (
                    'source',
                    models.SmallIntegerField(
                        choices=[
                            (1, 'Newly collected'),
                            (2, 'Reused from within the MPIB'),
                            (3, 'Reused from an external data provider'),
                        ],
                        default=1,
                        verbose_name='Data source',
                    ),
                ),
                (
                    'reuse_url',
                    models.URLField(
                        blank=True,
                        verbose_name='Original data source',
                        help_text='e.g., DOI, URL, research institution, contact person',
                    ),
                ),
                (
                    'collection_start',
                    study_registration.utils.fields.DateField(
                        blank=True, null=True, verbose_name='Start of data collection'
                    ),
                ),
                (
                    'collection_end',
                    study_registration.utils.fields.DateField(
                        blank=True, null=True, verbose_name='End of data collection'
                    ),
                ),
                (
                    'structure',
                    models.CharField(
                        blank=True,
                        choices=[
                            ('bids', 'BIDS'),
                            ('rps', 'R Package Structure'),
                            ('other', 'Other'),
                        ],
                        max_length=32,
                        verbose_name='Data structure',
                    ),
                ),
                (
                    'structure_documentation',
                    models.TextField(
                        blank=True,
                        help_text='If you do not use one of the established standards for data structure, please describe in detail how your data and metadata will be structured (e.g. naming conventions) and which tools you will use to ensure consistency.',
                        verbose_name='Description of structure',
                    ),
                ),
                (
                    'qa_measures',
                    models.TextField(
                        blank=True,
                        help_text='Which measures of quality assurance are taken for the data (e.g. checking of ID errors, movement, incomplete trials etc.)?',
                        verbose_name='Quality assurance measures',
                    ),
                ),
                (
                    'data_reuse',
                    models.SmallIntegerField(
                        blank=True,
                        choices=[
                            (1, 'Generally not possible'),
                            (2, 'Possible within the institute'),
                            (3, 'Possible for research purposes'),
                            (4, 'Generally possible'),
                        ],
                        help_text='Based on the informed consent that the participants have signed, which data reuse scenario is possible?',
                        null=True,
                        verbose_name='Data reuse',
                    ),
                ),
                (
                    'consent_location',
                    models.CharField(
                        blank=True,
                        help_text='Internal path to the signed informed consent forms, e.g. "FB-ARC/Projects/FaceHouse/Ethics/Consent" or "Cupboard in Dillenburger Str., Room 306."',
                        max_length=254,
                        verbose_name='Consent location',
                    ),
                ),
                (
                    'reuse_study',
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        related_name='+',
                        to='studies.study',
                        verbose_name='Study that the data you are reusing originated from',
                    ),
                ),
                (
                    'study',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to='studies.study'
                    ),
                ),
            ],
            options={
                'ordering': ['location'],
            },
        ),
    ]
