import re

RE = re.compile(' *([^ ]*):("[^"]*"|[^ "]*)')


def maybe_quote(value):
    if ' ' in str(value):
        return f'"{value}"'
    else:
        return value


def first_match(q, key):
    for m in RE.finditer(q):
        if m[1] == key:
            return m[0]


def setsearchquery(q, key, value):
    m = first_match(q, key)

    if value:
        token = f' {key}:{maybe_quote(value)}'
    else:
        token = ''

    if m:
        q = q.replace(m, token)
    else:
        q += token

    return q.strip()
