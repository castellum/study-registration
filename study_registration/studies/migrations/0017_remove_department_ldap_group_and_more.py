from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("studies", "0016_remove_department_enable_deployments"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="department",
            name="ldap_group",
        ),
        migrations.AddField(
            model_name="department",
            name="ldap_department",
            field=models.CharField(
                blank=True, max_length=254, verbose_name="LDAP department"
            ),
        ),
    ]
