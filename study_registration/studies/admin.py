from django.contrib import admin

from .models import Department
from .models import EthicsBoard
from .models import Keyword
from .models import Link
from .models import Project
from .models import Status
from .models import Study


class KeywordInline(admin.TabularInline):
    model = Keyword


class LinkInline(admin.TabularInline):
    model = Link


class StudyAdmin(admin.ModelAdmin):
    inlines = [KeywordInline, LinkInline]
    search_fields = ['title', 'short_title']
    filter_horizontal = ['owners']

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            kwargs['exclude'] = ['short_title']
        return super().get_form(request, obj, **kwargs)


admin.site.register(Status)
admin.site.register(Department)
admin.site.register(EthicsBoard)
admin.site.register(Project)
admin.site.register(Study, StudyAdmin)
