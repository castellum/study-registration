import datetime

from django.conf import settings
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import SuspiciousOperation
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView
from django.views.generic import DeleteView
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import UpdateView

from study_registration.studies.mixins import StudyAspectMixin
from study_registration.users.models import User
from study_registration.utils.mail import send_mail
from study_registration.utils.mail import send_study_mail

from . import gitlab
from .forms import DeploymentForm
from .models import Deployment


class DeploymentFormMixin(StudyAspectMixin):
    model = Deployment
    form_class = DeploymentForm

    def get_form(self, **kwargs):
        form = super().get_form(**kwargs)
        form.fields['target'].queryset = self.study.department.deploytarget_set.all()
        if not self.study.ethics_confirmed:
            form.fields['target'].queryset = (
                form.fields['target'].queryset.filter(ethics_required=False)
            )
        return form

    def form_valid(self, form):
        if not self.request.user.is_superuser:
            gitlab_id = form.cleaned_data['gitlab_id']
            if not gitlab.is_project_member(gitlab_id, self.request.user.username):
                raise SuspiciousOperation
        return super().form_valid(form)


class DeploymentCreateView(DeploymentFormMixin, CreateView):
    def send_notification(self):
        to = (
            User.objects
            .with_perm('deployments.approve', include_superusers=False)
            .exclude(email=None)
            .exclude(is_active=False)
            .values_list('email', flat=True)
        )

        subject = settings.EMAIL_SUBJECT_PREFIX + 'Deployment request'

        user = self.request.user.get_full_name()
        study = self.study.title
        link = self.request.build_absolute_uri(
            reverse('deployments:deploy', args=[
                self.study.pk, self.study.short_title, self.object.pk
            ])
        )
        body = (
            f'Your researcher {user} wants to publish their study "{study}"\n\n'
            f'View details here: {link}'
        )

        return send_mail(subject, body, None, to)

    def form_valid(self, form):
        form.instance.study = self.study
        response = super().form_valid(form)
        self.send_notification()
        return response


class DeploymentUpdateView(DeploymentFormMixin, UpdateView):
    def get_object(self):
        return get_object_or_404(
            super().get_queryset().filter(revisions=None),
            pk=self.kwargs['pk'],
        )


class DeploymentDeleteView(StudyAspectMixin, DeleteView):
    model = Deployment

    def get_object(self):
        return get_object_or_404(
            super().get_queryset().filter(revisions=None),
            pk=self.kwargs['pk'],
        )


class DeploymentListView(PermissionRequiredMixin, ListView):
    model = Deployment
    permission_required = ['deployments.approve']
    ordering = ['-pk']
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filters'] = [
            ('', _('All')),
            ('not-running', _('Not running')),
            ('running', _('Running')),
            ('expired', _('Expired')),
            ('rejected', _('Rejected')),
        ]
        return context

    def get_queryset(self):
        qs = super().get_queryset().select_related(
            'study__contact_person',
            'target',
        )
        today = datetime.date.today()
        if self.request.GET.get('filter') == 'rejected':
            return qs.filter(rejected=True)
        elif self.request.GET.get('filter') == 'not-running':
            return qs.filter(revisions=None, rejected=False)
        elif self.request.GET.get('filter') == 'running':
            return qs.exclude(revisions=None).filter(enddate__gte=today, rejected=False)
        elif self.request.GET.get('filter') == 'expired':
            return qs.exclude(revisions=None).filter(enddate__lt=today, rejected=False)
        else:
            return qs


class DeploymentRedeployView(StudyAspectMixin, UpdateView):
    model = Deployment
    template_name_suffix = '_redeploy'
    fields = [
        'git_ref',
        'git_file',
        'commit_sha',
        'startdate',
        'enddate',
    ]

    def get_object(self):
        return get_object_or_404(
            (
                super().get_queryset()
                .exclude(revisions=None)
                .exclude(enddate__lt=datetime.date.today())
            ),
            pk=self.kwargs['pk'],
        )

    def form_valid(self, form):
        form.instance.end_reminded = False
        response = super().form_valid(form)
        gitlab.create_deploy_branch(
            self.object.gitlab_id,
            self.object.generated_branch,
            self.object.commit_sha,
        )
        self.object.create_revision()
        return response


class DeploymentDeployView(PermissionRequiredMixin, StudyAspectMixin, DetailView):
    model = Deployment
    template_name_suffix = '_deploy'
    permission_required = ['deployments.approve']
    owner_required = False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['default_target_url'] = (
            self.object.target.url_template
            .replace('{study_id}', str(self.study))
            .replace('{deployment_id}', self.object.deployment_id)
            .replace('{study_id:-}', str(self.study).replace('_', '-'))
            .replace('{deployment_id:-}', self.object.deployment_id.replace('_', '-'))
        )
        return context

    def send_notification(self):
        study = self.study.title
        targeturl = self.object.targeturl
        link = self.request.build_absolute_uri(
            reverse('deployments:redeploy', args=[
                self.study.pk, self.study.short_title, self.object.pk
            ])
        )
        body = (
            f'Your study "{study}" has been deployed to the laboratory computers. '
            f'Your experiment is located in the path: {targeturl}.\n'
            f'View details here: {link}'
        )
        if self.request.POST.get('mail_comment'):
            body += '\n\n' + self.request.POST['mail_comment']

        approvers = (
            User.objects
            .with_perm('deployments.approve', include_superusers=False)
            .exclude(email=None)
            .values_list('email', flat=True)
        )

        return send_study_mail(
            'Deployment request processed',
            body,
            self.study,
            self.request,
            additional_to=approvers,
        )

    def post(self, request, **kwargs):
        self.object = self.get_object()

        if request.POST.get('action') == 'deploy':
            if self.object.deployed:
                raise SuspiciousOperation

            gitlab.create_deploy_branch(
                self.object.gitlab_id,
                self.object.generated_branch,
                self.object.commit_sha,
            )
            if self.object.target.gitlab_deploy_key is not None:
                gitlab.enable_deploy_key(
                    self.object.gitlab_id,
                    self.object.target.gitlab_deploy_key
                )
            self.object.create_revision()
        elif request.POST.get('action') == 'targeturl':
            if self.object.targeturl or not request.POST.get('targeturl'):
                raise SuspiciousOperation

            self.object.targeturl = request.POST.get('targeturl')
            self.object.save()
            self.send_notification()
        elif request.POST.get('action') == 'reject':
            self.object.rejected = True
            self.object.save()
        elif request.POST.get('action') == 'restore':
            self.object.rejected = False
            self.object.save()

        return redirect(request.path)
