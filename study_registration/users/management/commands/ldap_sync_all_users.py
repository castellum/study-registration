import ldap
from django.conf import settings
from django.contrib.auth.hashers import UNUSABLE_PASSWORD_PREFIX
from django.core.management.base import BaseCommand
from django.db import models

from study_registration.studies.models import Department
from study_registration.users.models import User


def iter_ldap_users():
    connection = ldap.initialize(settings.AUTH_LDAP_SERVER_URI)
    connection.simple_bind_s(
        settings.AUTH_LDAP_BIND_DN, settings.AUTH_LDAP_BIND_PASSWORD
    )

    q = settings.AUTH_LDAP_USER_SEARCH
    ctrl = ldap.controls.SimplePagedResultsControl(size=100, cookie='')

    while True:
        msgid = connection.search_ext(
            q.base_dn,
            q.scope,
            q.filterstr % {'user': '*'},
            serverctrls=[ctrl],
        )
        _, rdata, _, pctrls = connection.result3(msgid)

        yield from settings.AUTH_LDAP_USER_SEARCH._process_results(rdata)

        if pctrls and pctrls[0].cookie:
            ctrl.cookie = pctrls[0].cookie
        else:
            break


class Command(BaseCommand):
    def handle(self, *args, **options):
        to_be_created = []

        departments = Department.objects.exclude(ldap_department='')

        for dn, attrs in iter_ldap_users():
            do_sync = True

            if attrs.get('sAMAccountName', [''])[0].startswith('exp-'):
                do_sync = False
            elif (
                any(
                    attr not in attrs
                    for attr in ['sAMAccountName', 'givenName', 'sn', 'mail']
                )
                or int(attrs.get('userAccountControl', ['0'])[0], 10) & 2
                or 'computer' in attrs['objectClass']
            ):
                continue

            username = attrs['sAMAccountName'][0]
            user = User(username=username)

            if do_sync:
                for field, attr in settings.AUTH_LDAP_USER_ATTR_MAP.items():
                    if attr in attrs:
                        value = attrs[attr][0]
                        setattr(user, field, value)

                for department in departments:
                    if department.ldap_department in attrs.get('department', []):
                        user.default_department = department

            to_be_created.append(user)

        q_usable_password = ~models.Q(password='') & ~models.Q(
            password__startswith=UNUSABLE_PASSWORD_PREFIX
        )
        to_be_deleted = (
            User.objects
            .exclude(username__in=[u.username for u in to_be_created])
            .exclude(q_usable_password)
            .exclude(is_active=False)
        )

        old_count = User.objects.count()
        delete_count = to_be_deleted.count()

        User.objects.bulk_create(to_be_created, ignore_conflicts=True)
        to_be_deleted.update(is_active=False)

        new_count = User.objects.count()

        self.stdout.write(f'created {new_count - old_count} users')
        self.stdout.write(f'disabled {delete_count} users')
