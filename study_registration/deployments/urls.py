from django.urls import path

from .api import GitlabCommitsApiView
from .api import GitlabFilesApiView
from .api import GitlabProjectApiView
from .views import DeploymentCreateView
from .views import DeploymentDeleteView
from .views import DeploymentDeployView
from .views import DeploymentRedeployView
from .views import DeploymentUpdateView

app_name = 'deployments'
urlpatterns = [
    path('new/', DeploymentCreateView.as_view(), name='create'),
    path('<int:pk>/update/', DeploymentUpdateView.as_view(), name='update'),
    path('<int:pk>/delete/', DeploymentDeleteView.as_view(), name='delete'),
    path('<int:pk>/redeploy/', DeploymentRedeployView.as_view(), name='redeploy'),
    path('<int:pk>/deploy/', DeploymentDeployView.as_view(), name='deploy'),
    path('gitlab/project/', GitlabProjectApiView.as_view(), name='gitlab-project'),
    path('gitlab/commits/', GitlabCommitsApiView.as_view(), name='gitlab-commits'),
    path('gitlab/files/', GitlabFilesApiView.as_view(), name='gitlab-files'),
]
