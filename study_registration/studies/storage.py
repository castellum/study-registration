import requests
from django.conf import settings

from study_registration.utils import smb


def sync_group(study):
    if not settings.STORAGE_BASE_URL:
        return
    res = requests.post(
        f'{settings.STORAGE_BASE_URL}group/',
        json={
            'projectId': str(study),
            'manager': study.contact_person.username.lower(),
            'users': [u.username.lower() for u in study.owners.all()],
        },
        headers=settings.STORAGE_HEADERS,
        timeout=20,
    )
    res.raise_for_status()


def create_folder(study):
    if not settings.STORAGE_BASE_URL:
        if settings.DEBUG:
            return f'smb://exmaple.com/{study}'
        return None
    res = requests.post(
        f'{settings.STORAGE_BASE_URL}folder/',
        json={'projectId': str(study)},
        headers=settings.STORAGE_HEADERS,
        timeout=20,
    )
    res.raise_for_status()
    data = res.json()
    return smb.url(data['path'])


def get_quota(study):
    if not settings.STORAGE_BASE_URL:
        if settings.DEBUG:
            return {'quota': 5 << 30, 'usage': 2 << 30}
        return None
    res = requests.get(
        settings.STORAGE_BASE_URL,
        params={
            'projectId': str(study),
        },
        headers=settings.STORAGE_HEADERS,
        timeout=2,
    )
    res.raise_for_status()
    data = res.json()

    return data
