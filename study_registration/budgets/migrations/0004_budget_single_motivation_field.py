from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('budgets', '0003_replace_approve_flag_by_permission'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='budget',
            name='backgroundmotivation',
        ),
        migrations.RemoveField(
            model_name='budget',
            name='deliverable',
        ),
        migrations.RemoveField(
            model_name='budget',
            name='design',
        ),
        migrations.RemoveField(
            model_name='budget',
            name='researchquestion',
        ),
        migrations.RemoveField(
            model_name='budget',
            name='timeline',
        ),
        migrations.AddField(
            model_name='budget',
            name='motivation',
            field=models.TextField(
                blank=True,
                help_text='Please provide additional information about the planned study. What we would like to know is why you need to run the study and why there is no less expensive route. The document should contain sections on the study\'s theoretical background and motivation, research question and implications for the literature, design of the study, anticipated deliverables, and the timeline.',
                verbose_name='Motivation',
            ),
        ),
    ]
