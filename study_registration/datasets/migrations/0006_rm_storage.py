from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('datasets', '0005_remove_dataset_size'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='dataset',
            options={},
        ),
        migrations.RemoveField(
            model_name='dataset',
            name='location',
        ),
        migrations.RemoveField(
            model_name='dataset',
            name='to_be_stored_until_year',
        ),
    ]
