from django import forms

from .models import DataSet


class DataSetForm(forms.ModelForm):
    class Meta:
        model = DataSet
        fields = [
            'source',
            'reuse_study',
            'reuse_source',
            'collection_start',
            'collection_end',
            'structure',
            'structure_documentation',
            'data_reuse',
            'consent_location',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for key in ['reuse_study']:
            self.fields[key].empty_label = ''
            self.fields[key].widget.attrs.update({
                'data-select': True,
                'data-select-input-class': 'form-control',
                'data-select-value-class': 'badge text-bg-secondary badge-delete',
            })
