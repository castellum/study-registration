from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('studies', '0011_study_storage_archived'),
    ]

    operations = [
        migrations.AddField(
            model_name='study',
            name='imported_from',
            field=models.CharField(
                blank=True,
                max_length=254,
                verbose_name='Imported from',
                help_text='If anything is entered here, users will not be asked if they want to create a storage folder',
            ),
        ),
    ]
