from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("studies", "0017_remove_department_ldap_group_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="study",
            name="ethics_confirmed",
            field=models.DateField(
                blank=True, null=True, verbose_name="Ethics vote confirmed"
            ),
        ),
    ]
