from django.db import migrations, models


def migrate_data(apps, schema_editor):
    Department = apps.get_model('studies', 'Department')
    DeployTarget = apps.get_model('deployments', 'DeployTarget')
    for target in DeployTarget.objects.all():
        target.departments.set(Department.objects.filter(enable_deployments=True))


class Migration(migrations.Migration):
    dependencies = [
        ("studies", "0015_study_storage_deleted"),
        ("deployments", "0007_deployment_rejected"),
    ]

    operations = [
        migrations.AddField(
            model_name="deploytarget",
            name="departments",
            field=models.ManyToManyField(
                to="studies.department",
                verbose_name="Departments that can use this target",
            ),
        ),
        migrations.RunPython(migrate_data),
    ]
