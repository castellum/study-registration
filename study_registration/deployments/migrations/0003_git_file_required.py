from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('deployments', '0002_replace_approve_flag_by_permission'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deployment',
            name='git_file',
            field=models.CharField(
                help_text='A desktop shortcut to this file will be created on the laboratory computers.',
                max_length=256,
                verbose_name='Name of the file within the repository that starts your experiment',
            ),
        ),
        migrations.AlterField(
            model_name='deploymentrevision',
            name='git_file',
            field=models.CharField(
                help_text='A desktop shortcut to this file will be created on the laboratory computers.',
                max_length=256,
                verbose_name='Name of the file within the repository that starts your experiment',
            ),
        ),
    ]
