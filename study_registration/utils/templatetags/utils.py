import textwrap

from django import template
from django.utils.translation import gettext_lazy as _

from study_registration.utils import smb

register = template.Library()


@register.filter
def verbose_name(instance, field_name):
    field = instance._meta.get_field(field_name)
    try:
        return field.verbose_name
    except AttributeError:
        if field.one_to_many or field.many_to_many:
            return field.related_model._meta.verbose_name_plural
        else:
            return field.related_model._meta.verbose_name


@register.filter
def display(value, field_name=None):
    if field_name:
        getter = getattr(value, f'get_{field_name}_display', None)
        value = getter() if getter else getattr(value, field_name)

    if value is True:
        return _('Yes')
    elif value is False:
        return _('No')
    elif value is None or value == '':
        return '—'
    elif hasattr(value, 'all'):
        return ', '.join(str(x) for x in value.all()) or '—'
    else:
        return value


@register.inclusion_tag('utils/icon.html')
def icon(name, style='solid', *, label=''):
    return {'name': name, 'section': 'fa' + style[0], 'label': label}


@register.simple_tag(takes_context=True)
def smb_url_or_unc(context, s):
    try:
        return smb.url_or_unc(context.request, s)
    except TypeError:
        return s


@register.simple_tag
def wrapindent(text, width=70, indent=0):
    return '\n'.join(
        textwrap.fill(
            part,
            width=width,
            initial_indent=' ' * indent,
            subsequent_indent=' ' * indent,
        ) for part in text.splitlines()
    )


@register.filter
def format_bytes(value):
    for unit in ['B', 'KB', 'MB', 'GB', 'TB']:
        if value < 1000:
            return f'{value:.3g}\xa0{unit}'
        else:
            value /= 1024
    return f'{value:.3g}\xa0PB'
