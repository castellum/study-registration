from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("studies", "0013_ethics_board"),
    ]

    operations = [
        migrations.AddField(
            model_name="study",
            name="storage_managed",
            field=models.BooleanField(
                default=False,
                help_text="If selected, access permissions for the storage location will automatically be updated when owners change.",
                verbose_name="Storage is managed",
            ),
        ),
    ]
