from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('studies', '0002_initial2'),
    ]

    operations = [
        migrations.CreateModel(
            name='Publication',
            fields=[
                (
                    'id',
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                (
                    'doi',
                    models.CharField(
                        blank=True,
                        max_length=254,
                        null=True,
                        unique=True,
                        verbose_name='DOI',
                    ),
                ),
                ('title', models.CharField(max_length=254, verbose_name='Title')),
                ('authors', models.CharField(max_length=254, verbose_name='Authors')),
                ('journal', models.CharField(max_length=254, verbose_name='Journal')),
                (
                    'year',
                    models.IntegerField(
                        help_text='When adding a publication, please remember to update "to be stored until" for the data folder.',
                        verbose_name='Year',
                    ),
                ),
                (
                    'study',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to='studies.study',
                        verbose_name='Study',
                    ),
                ),
            ],
        ),
    ]
