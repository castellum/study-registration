from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('datasets', '0004_help_texts'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='dataset',
            name='size',
        ),
    ]
