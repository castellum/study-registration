from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("deployments", "0008_deploytarget_departments"),
    ]

    operations = [
        migrations.AddField(
            model_name="deployment",
            name="end_reminded",
            field=models.BooleanField(default=False),
        ),
    ]
