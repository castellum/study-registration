// expected elements:
// - data-formset="prefix"
// - on every item
//   - data-formset-item
//   - data-formset-rm-wrapper
// - only on the extra item
//   - data-formset-add-wrapper
//   - data-formset-add

var patchAttributes = function(root, attrs, patch) {
    for (var attr of attrs) {
        root.querySelectorAll(`[${attr}]`).forEach(el => {
            var oldValue = el.getAttribute(attr);
            var newValue = patch(oldValue);
            el.setAttribute(attr, newValue);
        });
    }
};

var clone = function(el) {
    var wrapper = document.createElement('div');
    wrapper.innerHTML = el.outerHTML;
    return wrapper.children[0];
}

var syncRemove = function(rm) {
    var item = rm.closest('[data-formset-item]');
    item.classList.toggle('formset-item-removed', rm.checked);
};

document.querySelectorAll('[data-formset-add]').forEach(button => {
    var formset = button.closest('[data-formset]');
    var prefix = formset.dataset.formset;
    var totalField = document.getElementById(`id_${prefix}-TOTAL_FORMS`);

    var item = button.closest('[data-formset-item]')
    var rmWrapper = item.querySelector('[data-formset-rm-wrapper]');
    rmWrapper.hidden = true;

    button.addEventListener('click', event => {
        var total = parseInt(totalField.value, 10);

        var oldItem = button.closest('[data-formset-item]');
        var newItem = clone(oldItem);

        patchAttributes(newItem, ['id', 'name', 'for'], value => {
            return value.replace(`${prefix}-${total - 1}`, `${prefix}-${total}`);
        })

        var oldRmWrapper = oldItem.querySelector('[data-formset-rm-wrapper]');
        oldRmWrapper.hidden = false;

        oldItem.after(newItem);

        var oldAddWrapper = oldItem.querySelector('[data-formset-add-wrapper]');
        var newAddWrapper = newItem.querySelector('[data-formset-add-wrapper]');
        newAddWrapper.after(oldAddWrapper);
        newAddWrapper.remove();

        totalField.value = total + 1;
    });

    formset.addEventListener('change', event => {
        if (event.target.matches('[data-formset-rm-wrapper] input')) {
            syncRemove(event.target);
        }
    });
    Array.from(formset.querySelectorAll('[data-formset-rm-wrapper] input')).forEach(rm => {
        syncRemove(rm);
    });
});
