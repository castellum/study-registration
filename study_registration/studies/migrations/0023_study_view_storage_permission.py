from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("studies", "0022_staff_fields"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="study",
            options={
                "ordering": ["-pk"],
                "permissions": [("view_storage", "Can view storage")],
            },
        ),
    ]
