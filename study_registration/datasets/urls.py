from django.urls import path

from .views import DataSetCreateView
from .views import DataSetDeleteView
from .views import DataSetUpdateView

app_name = 'publications'
urlpatterns = [
    path('new/', DataSetCreateView.as_view(), name='create'),
    path('<int:pk>/update/', DataSetUpdateView.as_view(), name='update'),
    path('<int:pk>/delete/', DataSetDeleteView.as_view(), name='delete'),
]
