var form = document.querySelector('main form');
var structure = form.structure;
var doc = form.structure_documentation;

var updateStructure = function() {
    var unstructured = structure.value === 'other';
    doc.disabled = !unstructured;
    doc.required = unstructured;
    doc.parentElement.hidden = !unstructured;
};

structure.addEventListener('change', updateStructure);
updateStructure();


var updateSource = function() {
    var selector = '.form-required [name]:enabled';
    document.querySelectorAll('[data-source]').forEach(pane => {
        if (pane.dataset.source === form.source.value) {
            pane.hidden = false;
            pane.querySelectorAll(selector).forEach(field => {
                field.required = true;
                // update validation on select/tag fields
                field.dispatchEvent(new Event('change'));
            });
        } else {
            pane.hidden = true;
            pane.querySelectorAll(selector).forEach(field => {
                field.required = false;
                // update validation on select/tag fields
                field.dispatchEvent(new Event('change'));
            });
        }
    });
}

document.addEventListener('change', (event) => {
    if (event.target.name === 'source') {
        updateSource();
    }
});
updateSource();
