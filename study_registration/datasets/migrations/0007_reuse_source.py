from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datasets', '0006_rm_storage'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='dataset',
            name='reuse_url',
        ),
        migrations.AddField(
            model_name='dataset',
            name='reuse_source',
            field=models.CharField(
                blank=True,
                help_text='e.g., DOI, URL, research institution, contact person',
                max_length=254,
                verbose_name='Original data source',
            ),
        ),
    ]
