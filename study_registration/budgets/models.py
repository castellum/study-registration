from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

from study_registration.studies.models import Study


class Budget(models.Model):
    study = models.ForeignKey(Study, on_delete=models.CASCADE, verbose_name=_('Study'))
    expected = models.DecimalField(
        _('Expected cost (€)'), max_digits=12, decimal_places=2
    )
    actual = models.DecimalField(
        _('Actual cost (€)'), max_digits=12, decimal_places=2, blank=True, null=True
    )

    motivation = models.TextField(_('Motivation'), help_text=_(
        'Please provide additional information about the planned study. '
        'What we would like to know is why you need to run the study '
        'and why there is no less expensive route. '
        "The document should contain sections on the study's "
        'theoretical background and motivation, '
        'research question and implications for the literature, '
        'design of the study, '
        'anticipated deliverables, '
        'and the timeline.'
    ), blank=True)

    approved_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('Approved by'),
        on_delete=models.PROTECT,
        related_name='+',
        blank=True,
        null=True,
    )

    class Meta:
        permissions = [
            ('approve', _('Can approve')),
        ]

    def needs_approval(self):
        return self.expected >= settings.BUDGETS_APPROVAL_THRESHOLD
