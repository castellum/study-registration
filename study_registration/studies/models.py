import datetime
import re
from urllib.parse import urlencode

from django.conf import settings
from django.core import validators
from django.db import models
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _

from study_registration.utils.fields import DateField


class Status(models.Model):
    name = models.CharField(_('Name'), max_length=254, unique=True)
    color = models.CharField(_('Color'), max_length=32, choices=[
        ('primary', _('primary')),
        ('secondary', _('secondary')),
        ('success', _('success')),
        ('danger', _('danger')),
        ('warning', _('warning')),
        ('info', _('info')),
        ('light', _('light')),
        ('dark', _('dark')),
    ])

    def __str__(self):
        return self.name


class Department(models.Model):
    name = models.CharField(_('Name'), max_length=254, unique=True)
    ldap_department = models.CharField(_('LDAP department'), max_length=254, blank=True)
    enable_budgets = models.BooleanField(_('Enable budgets'), default=False)
    is_active = models.BooleanField(_('active'), default=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

    @cached_property
    def enable_deployments(self):
        return self.deploytarget_set.exists()


class Project(models.Model):
    created_at = models.DateField(_('Created at'), auto_now_add=True)
    title = models.CharField(_('Title'), max_length=255, unique=True)

    class Meta:
        ordering = ['title']

    def __str__(self):
        return self.title


class EthicsBoard(models.Model):
    name = models.CharField(_('Name'), max_length=254, unique=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class StudyQuerySet(models.QuerySet):
    def get_by_key(self, key):
        m = re.match('([0-9]+)-([-a-zA-Z0-9_]+)', key)
        if not m:
            raise Study.DoesNotExist
        return self.get(pk=int(m[1], 10), short_title=m[2])


class Study(models.Model):
    created_at = models.DateField(_('Created at'), auto_now_add=True)
    updated_at = models.DateField(_('Updated at'), auto_now=True)
    imported_from = models.CharField(
        _('Imported from'),
        max_length=254,
        blank=True,
        help_text=_(
            'If anything is entered here, users will not be asked if '
            'they want to create a storage folder'
        ),
    )
    owners = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        limit_choices_to={'is_active': True},
        verbose_name=_(
            'Users who can edit this study profile and access the private folder'
        ),
    )

    title = models.CharField(_('Title'), max_length=254)
    short_title = models.CharField(
        _('Short title'),
        help_text=_(
            'The short title will be used to refer to this study within '
            'the institute context, e.g., to create a physical folder '
            'on the server. Therefore, only use lower-case letters, numbers, '
            'or hyphens.'
        ),
        max_length=254,
        validators=[
            validators.RegexValidator(
                r'^[-a-z0-9]+\Z',
                _('Only use lower-case letters, numbers, and hyphens.')
            ),
            validators.MaxLengthValidator(16),
        ],
    )
    aliases = models.CharField(
        _('Other titles or acronyms'),
        help_text=_(
            'Please compile a complete list of additional titles that '
            'may refer to your study, including e.g. a German title '
            'used in communication with participants.'
        ),
        max_length=254,
        blank=True,
    )
    end = DateField(
        _('Expected end of study'),
        help_text=_(
            'When analyses are completed, manuscripts are submitted, and the data is '
            'no longer in active use, it should be moved to an archive in order to '
            'free up storage capacity. Entering this date allows the Rasearch Data '
            'Management Team to contact you to coordinate the archiving process.'
        ),
        blank=True,
        null=True,
    )
    description = models.TextField(
        _('Description'),
        help_text=_(
            'This text should allow other researchers to quickly grasp whether this '
            'study and the related data could be of interest to them.'
        ),
        blank=True,
    )
    department = models.ForeignKey(
        Department, verbose_name=_('Department'), on_delete=models.CASCADE
    )
    project = models.ForeignKey(
        Project,
        verbose_name=_('Project'),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )

    contact_person = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('Contact person'),
        help_text=_(
            'The contact person must be reachable. Before the current contact person '
            'leaves the institute, they must pass on the study to someone else.'
        ),
        on_delete=models.PROTECT,
        limit_choices_to={
            'is_active': True,
        },
        related_name='+',
    )
    affiliated_scientists = models.CharField(
        _('Affiliated scientists'),
        max_length=254,
        help_text=_(
            'You can use the same format as you would use for the authors of a paper.'
        ),
    )

    ethics_board = models.ForeignKey(
        EthicsBoard,
        verbose_name=_('Ethics board'),
        help_text=_(
            'Please remember to save the application and approval to the study folder.'
        ),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )
    ethics_confirmed = models.DateField(
        _('Ethics vote confirmed'), blank=True, null=True
    )

    storage_location = models.CharField(
        _('Storage location'), max_length=255, blank=True, null=True, unique=True
    )
    storage_archived = models.DateField(_('Storage archived'), blank=True, null=True)
    storage_archive_name = models.CharField(
        _('Storage archive name'), max_length=255, blank=True
    )
    storage_deleted = models.DateField(
        _('Storage deleted'),
        help_text=_(
            'The data is no longer available, neither from the storage '
            'location nor the archive.'
        ),
        blank=True,
        null=True,
    )
    ldap_group_managed = models.BooleanField(
        _('LDAP group is managed'),
        help_text=_(
            'If selected, access permissions for the storage location will '
            'automatically be updated when owners change.'
        ),
        default=False,
    )
    ldap_group_synced = models.BooleanField(_('LDAP group is synced'), default=False)

    staff_followup = DateField(_('Follow-Up'), blank=True, null=True)
    staff_status = models.ForeignKey(
        Status,
        verbose_name=_('Status'),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )

    objects = StudyQuerySet.as_manager()

    def get_storage_archive_name(self):
        if self.storage_archive_name:
            return self.storage_archive_name
        elif self.storage_archived:
            year = self.storage_archived.year
            quarter = ((self.storage_archived.month - 1) // 3) + 1
            return f'ArchivSRT_{year}-Q{quarter}'

    class Meta:
        ordering = ['-pk']
        permissions = [
            ('view_storage', _('Can view storage')),
        ]

    def __str__(self):
        return f'{self.pk}-{self.short_title}'

    def get_absolute_url(self):
        return reverse(
            'studies:detail', kwargs={'pk': self.pk, 'slug': self.short_title}
        )

    def data_reused_by(self):
        return Study.objects.filter(dataset__reuse_study=self).distinct().order_by('pk')

    @property
    def storage_is_archived(self):
        today = datetime.date.today()
        return self.storage_archived and self.storage_archived < today

    @property
    def storage_is_deleted(self):
        today = datetime.date.today()
        return self.storage_deleted and self.storage_deleted < today

    @property
    def storage_request_url(self):
        if not settings.CREATE_TICKET_URL:
            return None
        return settings.CREATE_TICKET_URL + '?' + urlencode({
            'summary': f'[Quota] Request to increase Isilon quota for study {self}',
            'description': (
                '[Please describe how much storage space you need and why you need it.]'
            ),
        })


class Keyword(models.Model):
    study = models.ForeignKey(Study, on_delete=models.CASCADE)
    name = models.CharField(_('Name'), max_length=128)

    class Meta:
        unique_together = [['study', 'name']]

    def __str__(self):
        return self.name


class Link(models.Model):
    WEBSITE = 1
    PREREG = 2
    DATA = 3
    CODE = 4

    study = models.ForeignKey(Study, on_delete=models.CASCADE)
    url = models.URLField(_('URL'))
    type = models.SmallIntegerField(_('Type'), choices=[
        (WEBSITE, _('Website')),
        (PREREG, _('Preregistration')),
        (DATA, _('Public data repository')),
        (CODE, _('Public code repository')),
    ])

    class Meta:
        ordering = ['type', 'pk']
