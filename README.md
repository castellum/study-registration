This is a study registration tool for internal use at MPIB.

# Quickstart

Run `make` to start a development setup. python and npm are required.

For members of MPIB, a demo server is available at
<https://studies.t.mpib-berlin.mpg.de>.

# Goals

-   House-wide overview of running studies
-   Encourage collaboration and data reuse between researchers
-   Encourage Open Science practices
-   Provide a clear structure and useful information to junior
    researchers which will save them time and nerves later
-   Allow data stewards to proactively approach researchers in relevant
    phases of the study life cycle
-   Provide a single source of truth for the storage location of all
    research data
-   Automate repetitive tasks. For example, this tool provides a
    permanent URL for each study that can be referenced in other
    internal tools (e.g. Castellum) so researchers do not have to repeat
    the same information over and over.
-   Provide information to IT so they can more effectively use the
    available storage capacity.

# Non-Goals

-   This is not a hosting platform for public datasets. You can use
    public repositories such as <https://osf.io> for that.
-   This should not be a power-tool that takes practice to master.
    Instead, it should be intuitive and stay out of the way.

# Architecture

The `studies` app is at the center. Most other apps add additional
information to studies.

Even though that approach is already quite modular, the code is
currently not fully decoupled. That could be changed by using some
inversion-of-control pattern. But it is unclear at this point whether
the added complexity would be worth it.
