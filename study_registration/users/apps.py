from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'study_registration.users'

    def ready(self):
        try:
            from django_auth_ldap.backend import populate_user

            from .signals import ldap_set_department

            populate_user.connect(ldap_set_department)
        except ImportError:
            pass
