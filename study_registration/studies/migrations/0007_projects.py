from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('studies', '0006_alter_study_end'),
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                (
                    'id',
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                (
                    'created_at',
                    models.DateField(auto_now_add=True, verbose_name='Created at'),
                ),
                (
                    'title',
                    models.CharField(max_length=255, unique=True, verbose_name='Title'),
                ),
            ],
            options={
                'ordering': ['title'],
            },
        ),
        migrations.AddField(
            model_name='study',
            name='project',
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to='studies.project',
                verbose_name='Project',
            ),
        ),
    ]
