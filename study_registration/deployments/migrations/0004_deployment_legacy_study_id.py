from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("deployments", "0003_git_file_required"),
    ]

    operations = [
        migrations.AddField(
            model_name="deployment",
            name="legacy_study_id",
            field=models.CharField(
                blank=True, max_length=254, verbose_name="Legacy study ID"
            ),
        ),
    ]
