from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('deployments', '0004_deployment_legacy_study_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='deployment',
            name='status',
        ),
    ]
