from xml.sax.saxutils import XMLGenerator

from django.http import HttpResponse


class XMLResponse(HttpResponse):
    def __init__(self, tree, **kwargs):
        kwargs.setdefault('content_type', 'application/xml')
        super().__init__(**kwargs)
        xml = XMLGenerator(self, 'UTF-8')
        xml.startDocument()
        self._xml(xml, *tree)
        xml.endDocument()

    def _xml(self, xml, tag, attrs={}, children=[]):
        xml.startElement(tag, attrs)
        if isinstance(children, str):
            xml.characters(children)
        elif children:
            for child in children:
                if child:
                    self._xml(xml, *child)
        xml.endElement(tag)


def user_name(user):
    if user.first_name and user.last_name:
        return f'{user.last_name}, {user.first_name}'
    else:
        return user.get_username()


def field(tag, ind1, ind2, subfields):
    return (
        'datafield',
        {'tag': tag, 'ind1': ind1, 'ind2': ind2},
        [('subfield', {'code': code}, value) for code, value in subfields.items()],
    )


def record(request, study):
    # see https://www.loc.gov/marc/bibliographic/
    return ('record', {}, [
        ('leader', {}, '00000naa a2200000uub4500'),
        ('controlfield', {'tag': '001'}, str(study)),
        ('controlfield', {'tag': '003'}, 'de-b1532'),
        ('controlfield', {'tag': '005'}, study.updated_at.strftime('%Y%m%d')),
        field('100', '1', ' ', {'a': user_name(study.contact_person)}),
        field('210', '0', ' ', {'a': study.short_title}),
        field('245', '1', '0', {'a': study.title}),
        study.aliases and field('246', '0', '3', {'a': study.aliases}),
        field('260', ' ', ' ', {'c': study.created_at.isoformat()}),
        study.description and field('520', ' ', ' ', {'a': study.description}),
        *[
            field('700', '1', ' ', {'a': user_name(user)})
            for user in study.owners.all()
            if user != study.contact_person
        ],
        field('856', '4', ' ', {
            'u': request.build_absolute_uri(study.get_absolute_url())
        }),
        *[
            field('856', '4', ' ', {'u': link.url, '3': link.get_type_display()})
            for link in study.link_set.all()
        ],
        *[
            field('856', '4', ' ', {'u': p.doi_url(), '3': 'Publication'})
            for p in study.publication_set.all() if p.doi
        ],
        field('996', ' ', ' ', {'a': 'Study'}),
    ])


def marcxml(request, studies):
    return XMLResponse((
        'collection',
        {'xmlns': 'http://www.loc.gov/MARC21/slim'},
        [record(request, study) for study in studies],
    ))
