# https://docs.gitlab.com/ce/api/rest/
# FIXME: consider pagination

import requests
from django.conf import settings


def urljoin(*parts):
    return '/'.join(str(p).strip('/') for p in parts)


def _request(*path, params={}, method='GET'):
    url = urljoin(settings.DEPLOYMENTS_GITLAB_BASE_URL, '/api/v4/projects', *path)
    res = requests.request(
        method,
        url,
        params=params,
        headers=settings.DEPLOYMENTS_GITLAB_HEADERS,
        timeout=10,
    )
    res.raise_for_status()
    return res


def get_project(project_id):
    return _request(project_id).json()


def is_project_member(project_id, username):
    members = _request(project_id, 'members/all').json()
    return any(m['username'].lower() == username.lower() for m in members)


def get_branches(project_id):
    branches = _request(project_id, 'repository/branches').json()
    return [b for b in branches if not b['name'].startswith('asr_deploy_')]


def get_commits(project_id, ref):
    return _request(project_id, 'repository/commits', params={'ref_name': ref}).json()


def get_files(project_id, ref):
    files = _request(project_id, 'repository/tree', params={
        'ref': ref,
        'recursive': True,
        'per_page': 200,
    }).json()
    return [f['path'] for f in files]


def enable_deploy_key(project_id, key_id):
    return _request(project_id, 'deploy_keys', key_id, 'enable', method='POST').json()


def create_deploy_branch(project_id, branch, ref):
    # unprotect old branch
    try:
        _request(project_id, 'protected_branches', branch, method='DELETE')
    except requests.HTTPError as e:
        if e.response.status_code != 404:
            raise

    # delete old branch
    try:
        _request(project_id, 'repository/branches', branch, method='DELETE')
    except requests.HTTPError as e:
        if e.response.status_code != 404:
            raise

    # create new branch
    _request(project_id, 'repository/branches', params={
        'branch': branch,
        'ref': ref,
    }, method='POST')

    # protect branch
    _request(project_id, 'protected_branches', params={
        'name': branch,
        'push_access_level': 60,
        'unprotect_access_level': 60,
    }, method='POST')
