from django.test import TestCase

from model_bakery import baker

from study_registration.studies.models import Study
from study_registration.studies.views import StudyListView


class StudySearchScoreTest(TestCase):
    def assert_score(self, study, q, score):
        view = StudyListView()
        qs = view.apply_score(Study.objects.all(), q)
        scored_study = qs.get(pk=study.pk)
        self.assertEqual(scored_study.score, score)

    def test_exact_title(self):
        study = baker.make(Study, title='fooooo')
        self.assert_score(study, 'fooooo', 150)

    def test_partial_title(self):
        study = baker.make(Study, title='fooooo baaaar')
        self.assert_score(study, 'fooooo', 50)

    def test_exact_title_with_keywords(self):
        study = baker.make(Study, title='fooooo')
        study.keyword_set.create(name='1')
        study.keyword_set.create(name='2')
        self.assert_score(study, 'fooooo', 150)

    def test_multiple_words_in_q(self):
        study = baker.make(Study, title='fooooo baaaar')
        self.assert_score(study, 'fooooo baaaar', 50 * 50)

    def test_exact_keyword(self):
        study = baker.make(Study)
        study.keyword_set.create(name='fooooo')
        study.keyword_set.create(name='baaaar')
        self.assert_score(study, 'fooooo', 7)

    def test_multiple_keywords(self):
        study = baker.make(Study)
        study.keyword_set.create(name='fooooo')
        study.keyword_set.create(name='baaaar')
        self.assert_score(study, 'fooooo baaaar', 7 * 7)

    def test_non_unique_keyword(self):
        study = baker.make(Study)
        study.keyword_set.create(name='fooooo1')
        study.keyword_set.create(name='fooooo2')
        self.assert_score(study, 'fooooo', 2 + 2)
