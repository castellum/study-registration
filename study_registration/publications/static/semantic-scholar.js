import makeSpinner from './spinner.js'

var doi = document.querySelector('#id_doi');
var title = document.querySelector('#id_title');
var authors = document.querySelector('#id_authors');
var journal = document.querySelector('#id_journal');
var year = document.querySelector('#id_year');

var toggleSpinner = makeSpinner(doi);

var delay = function(fn, timeout) {
    var timeoutId;
    return function() {
        clearTimeout(timeoutId);
        timeoutId = setTimeout(fn, timeout);
    };
};

var normalizeDOI = function(doi) {
    return doi
        .replace('doi:', '')
        .replace('https://doi.org/', '')
        .trim();
};

document.querySelector('#id_doi').addEventListener('input', delay(event => {
    if (doi.value) {
        // https://api.semanticscholar.org/api-docs/graph#operation/get_graph_get_paper_search
        var url = 'https://api.semanticscholar.org/graph/v1/paper/'
            + encodeURI(normalizeDOI(doi.value))
            + '?fields=title,url,externalIds,authors,abstract,journal,year';

        toggleSpinner(true);
        fetch(url).then(response => {
            if (response.ok) {
                return response.json();
            } else {
                throw response;
            }
        }).then(data => {
            title.value = data.title;
            authors.value = data.authors.map(a => a.name).join(', ');
            journal.value = data.journal.name;
            year.value = data.year;
        }).catch(error => {
            title.value = '';
            authors.value = '';
            journal.value = '';
            year.value = '';
            throw error;
        }).finally(() => {
            toggleSpinner(false);
        });
    }
}, 500));
