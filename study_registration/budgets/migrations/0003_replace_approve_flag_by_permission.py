from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('budgets', '0002_approval'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='budget',
            options={'permissions': [('approve', 'Can approve')]},
        ),
        migrations.AlterField(
            model_name='budget',
            name='approved_by',
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name='+',
                to=settings.AUTH_USER_MODEL,
                verbose_name='Approved by',
            ),
        ),
    ]
