from django.db import migrations
import study_registration.utils.fields


class Migration(migrations.Migration):

    dependencies = [
        ('studies', '0005_department_ldap_group'),
    ]

    operations = [
        migrations.AlterField(
            model_name='study',
            name='end',
            field=study_registration.utils.fields.DateField(
                blank=True,
                help_text='When analyses are completed, manuscripts are submitted, and the data is no longer in active use, it should be moved to an archive in order to free up storage capacity. Entering this date allows the Rasearch Data Management Team to contact you to coordinate the archiving process.',
                null=True,
                verbose_name='Expected end of study',
            ),
        ),
    ]
