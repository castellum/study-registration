from bootstrap_colors.views import BootstrapColorsView
from django.contrib import admin
from django.contrib.auth.views import LoginView
from django.contrib.auth.views import LogoutView
from django.urls import include
from django.urls import path
from django.views.decorators.cache import cache_control
from django.views.generic import RedirectView
from django.views.generic import TemplateView

from study_registration.budgets.views import BudgetListView
from study_registration.deployments.api import DeploymentsApiView
from study_registration.deployments.views import DeploymentListView

urlpatterns = [
    path('', RedirectView.as_view(pattern_name='studies:list'), name='index'),
    path('about/', TemplateView.as_view(template_name='about.html'), name='about'),
    path(
        'contact/', TemplateView.as_view(template_name='contact.html'), name='contact'
    ),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('admin/', admin.site.urls),
    path('colors.css', cache_control(max_age=86400)(
        BootstrapColorsView.as_view()
    ), name='colors'),
    path(
        'studies/<int:study_pk>-<slug:study_short>/publications/',
        include('study_registration.publications.urls', namespace='publications'),
    ),
    path(
        'studies/<int:study_pk>-<slug:study_short>/datasets/',
        include('study_registration.datasets.urls', namespace='datasets'),
    ),
    path(
        'studies/<int:study_pk>-<slug:study_short>/budgets/',
        include('study_registration.budgets.urls', namespace='budgets'),
    ),
    path(
        'studies/<int:study_pk>-<slug:study_short>/deployments/',
        include('study_registration.deployments.urls', namespace='deployments'),
    ),
    path(
        'studies/<int:study_pk>-<slug:study_short>/biosamples/',
        include('study_registration.biosamples.urls', namespace='biosamples'),
    ),
    path('studies/', include('study_registration.studies.urls', namespace='studies')),
    path('budgets/', BudgetListView.as_view(), name='budget-list'),
    path('deployments/', DeploymentListView.as_view(), name='deployment-list'),
    path('api/deployments/', DeploymentsApiView.as_view(), name='deployment-api'),
]

try:
    import debug_toolbar

    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]
except ImportError:
    pass
