import datetime
import logging
from urllib.parse import urlencode

from django import forms
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import SuspiciousOperation
from django.db import models
from django.db.models.functions import Coalesce
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.feedgenerator import DefaultFeed
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView
from django.views.generic import DeleteView
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import RedirectView
from django.views.generic import TemplateView
from django.views.generic import UpdateView
from django.views.generic import View

from study_registration.datasets.models import DataSet
from study_registration.users.mixins import StaffRequiredMixin
from study_registration.users.models import User
from study_registration.utils import smb
from study_registration.utils.mail import send_mail
from study_registration.utils.mail import send_study_mail
from study_registration.utils.search import setsearchquery

from . import storage
from .forms import StudyCreateForm
from .forms import StudyUpdateForm
from .marcxml import marcxml
from .models import Department
from .models import EthicsBoard
from .models import Keyword
from .models import Link
from .models import Project
from .models import Study

logger = logging.getLogger(__name__)
request_logger = logging.getLogger('django.request')


def ensure_date(date_or_datetime):
    if isinstance(date_or_datetime, datetime.datetime):
        return date_or_datetime.date()
    else:
        return date_or_datetime


def split_with_quotes(s):
    part = ''
    quoted = False
    for c in s:
        if quoted:
            if c == '"':
                quoted = False
            else:
                part += c
        else:
            if c == ' ':
                if part:
                    yield part
                    part = ''
            elif c == '"':
                quoted = True
            else:
                part += c
    if part:
        yield part


def make_score(value, **kwargs):
    return models.Value(value) * models.Count('pk', filter=models.Q(**kwargs))


def make_keyword_score(value, **kwargs):
    return models.Value(value) * Coalesce((
        Keyword.objects
        .filter(study=models.OuterRef('pk'), **kwargs)
        .order_by()
        .values('study')
        .annotate(count=models.Count('study'))
        .values('count')
    ), 0)


class OwnerRequiredMixin(LoginRequiredMixin):
    def get_queryset(self):
        if self.request.user.is_superuser:
            return Study.objects.all()
        else:
            return self.request.user.study_set.all()


class StudyListView(ListView):
    model = Study
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        def choices(model, field):
            return [
                (name, name) for name in model.objects.values_list(field, flat=True)
            ]

        reuse = DataSet._meta.get_field('data_reuse')

        context['filters'] = [
            ('status', _('Status'), [
                ('running', _('Running')),
                ('finished', _('Finished')),
            ]),
            ('department', _('Department'), choices(Department, 'name')),
            ('project', _('Project'), choices(Project, 'title')),
            ('ethicsboard', _('Ethics board'), choices(EthicsBoard, 'name')),
            ('reuse', reuse.verbose_name, reuse.choices[1:]),
            ('open', _('Open Science'), [
                ('prereg', _('Preregistration')),
                ('data', _('Public data repository')),
                ('code', _('Public code repository')),
            ]),
        ]

        context['user_list'] = self.get_user_list()

        return context

    def get_user_list(self):
        q = self.request.GET.get('q')
        if not q:
            return

        for part in split_with_quotes(q):
            if ':' in part:
                key, value = part.split(':', 1)
                if key in ['contact', 'owner']:
                    return User.objects.filter(username=value, is_active=True)

        if ' ' not in q:
            matches = User.objects.filter(username__icontains=q, is_active=True)
            if matches.count() <= 3:
                return matches

    def apply_score(self, qs, q):  # noqa: C901
        # scores are multiplied, so they can get really big; better use a float
        score = models.Value(1, output_field=models.FloatField())
        for part in split_with_quotes(q):
            if ':' in part:
                key, value = part.split(':', 1)
                if key == 'contact':
                    qs = qs.filter(contact_person__username=value)
                    continue
                elif key == 'owner':
                    qs = qs.filter(owners__username=value)
                    continue
                elif key == 'department':
                    qs = qs.filter(department__name=value)
                    continue
                elif key == 'project':
                    qs = qs.filter(project__title=value)
                    continue
                elif key == 'keyword':
                    qs = qs.filter(keyword__name=value)
                    continue
                elif key == 'doi':
                    qs = qs.filter(publication__doi=value)
                    continue
                elif key == 'status' and value == 'running':
                    qs = qs.exclude(end__lt=datetime.date.today())
                    continue
                elif key == 'status' and value == 'finished':
                    qs = qs.filter(end__lt=datetime.date.today())
                    continue
                elif key == 'reuse':
                    qs = qs.filter(dataset__data_reuse__gte=value)
                    continue
                elif key == 'ethicsboard':
                    qs = qs.filter(ethics_board__name=value)
                    continue
                elif key == 'open' and value == 'prereg':
                    qs = qs.filter(has_preregistration=True)
                    continue
                elif key == 'open' and value == 'data':
                    qs = qs.filter(has_open_data=True)
                    continue
                elif key == 'open' and value == 'code':
                    qs = qs.filter(has_open_materials=True)
                    continue

            score *= sum([
                make_score(100, title__iexact=part),
                make_score(100, short_title__iexact=part),
                make_score(100, publication__doi=part),
                make_score(50, title__icontains=part),
                make_score(20, contact_person__username__icontains=part),
                make_score(10, affiliated_scientists__icontains=part),
                make_score(5, aliases__icontains=part),
                make_score(5, department__name__iexact=part),
                make_keyword_score(5, name__iexact=part),
                make_keyword_score(2, name__icontains=part),
                make_score(1, description__icontains=part),
                make_score(1, contact_person__first_name__icontains=part),
            ])

        return qs.annotate(score=score)

    def get_queryset(self):
        qs = (
            super().get_queryset()
            .select_related('contact_person')
            .prefetch_related('keyword_set')
        )

        qs = qs.annotate(
            has_preregistration=models.Exists(
                Link.objects.filter(study=models.OuterRef('pk'), type=Link.PREREG)
            ),
            has_open_data=models.Exists(
                Link.objects.filter(study=models.OuterRef('pk'), type=Link.DATA)
            ),
            has_open_materials=models.Exists(
                Link.objects.filter(study=models.OuterRef('pk'), type=Link.CODE)
            ),
            has_reuse=models.Exists(
                DataSet.objects.filter(reuse_study=models.OuterRef('pk'))
            ),
        )

        q = self.request.GET.get('q')
        if q:
            qs = self.apply_score(qs, q)
            qs = qs.filter(score__gt=0).order_by('-score', '-created_at')
        else:
            qs = qs.order_by('-created_at')

        return qs.distinct()

    def feed(self, request, size=20):
        feed = DefaultFeed(
            title='Study Registration',
            link=request.build_absolute_uri('/'),
            description='',
            feed_url=request.build_absolute_uri(request.get_full_path()),
        )

        for study in self.get_queryset()[:size]:
            feed.add_item(
                title=study.title,
                link=request.build_absolute_uri(study.get_absolute_url()),
                unique_id=request.build_absolute_uri(study.get_absolute_url()),
                description=study.description,
                pubdate=study.created_at,
                updateddate=study.updated_at,
                author_name=study.contact_person.get_full_name(),
                author_email=study.contact_person.email,
            )

        response = HttpResponse(content_type=feed.content_type)
        feed.write(response, 'utf-8')
        return response

    def normalize_query(self, q):
        # if any other field is provided, encode it into q and redirect
        modified = False
        query = {}
        for key, value in self.request.GET.items():
            if key in ['page', 'format']:
                query[key] = value
            elif key != 'q':
                q = setsearchquery(q, key, value)
                modified = True
        if modified:
            query['q'] = q
            return query

    def get(self, request, **kwargs):
        # if q is an exact study ID, redirect there directly
        q = request.GET.get('q', '')
        try:
            study = super().get_queryset().get_by_key(q)
            return redirect(study.get_absolute_url())
        except Study.DoesNotExist:
            pass

        normalized = self.normalize_query(q)
        if normalized:
            return redirect(request.path + '?' + urlencode(normalized))

        if request.GET.get('format') == 'csv':
            response = HttpResponse(content_type='text/plain')
            for study in self.get_queryset():
                response.write(f'{study}\n')
            return response
        elif request.GET.get('format') == 'mrcx':
            return marcxml(request, (
                self.get_queryset()
                .select_related('contact_person')
                .prefetch_related('owners', 'link_set', 'publication_set')
            ))
        elif request.GET.get('format') == 'feed':
            return self.feed(request)

        return super().get(request, **kwargs)


class MyStudiesView(LoginRequiredMixin, RedirectView):
    def get_redirect_url(self):
        return '{}?{}'.format(reverse('studies:list'), urlencode({
            'q': 'owner:' + self.request.user.username,
        }))


class StudyDetailView(DetailView):
    model = Study
    slug_field = 'short_title'
    query_pk_and_slug = True

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['is_owner'] = (
            self.request.user.is_superuser
            or self.object.owners.filter(pk=self.request.user.pk).exists()
        )
        context['last_publication_year'] = self.object.publication_set.aggregate(
            year=models.Max('year')
        )['year']
        if context['last_publication_year']:
            context['publication_to_be_stored_until'] = (
                context['last_publication_year'] + 10
            )
        if (
            self.object.ldap_group_managed
            and not self.object.storage_is_archived
            and not self.object.storage_is_deleted
            and (
                context['is_owner']
                or self.request.user.has_perm('studies.view_storage')
            )
        ):
            try:
                context['quota'] = storage.get_quota(self.object)
            except Exception:
                logger.exception('failed to get quota')
        return context


class StudyRedirectView(RedirectView):
    def get_redirect_url(self, pk):
        study = get_object_or_404(Study, pk=pk)
        return study.get_absolute_url()


class LinkFormsetMixin:
    def get_formset(self):
        formset_class = forms.modelformset_factory(
            Link, fields=['url', 'type'], extra=1, can_delete=True
        )
        if self.object:
            qs = self.object.link_set.all()
        else:
            qs = Link.objects.none()
        return formset_class(queryset=qs, data=self.get_form_kwargs().get('data'))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'formset' not in context:
            context['formset'] = self.get_formset()
        return context

    def form_valid(self, form, formset):
        response = super().form_valid(form)
        for f in formset.extra_forms:
            f.instance.study = self.object
        formset.save()
        return response

    def form_invalid(self, form, formset):
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )


class StudyCreateView(OwnerRequiredMixin, LinkFormsetMixin, CreateView):
    model = Study
    form_class = StudyCreateForm

    def send_notification(self):
        study = str(self.object)
        user = self.request.user.get_full_name()
        link = self.request.build_absolute_uri(self.object.get_absolute_url())

        subject = settings.EMAIL_SUBJECT_PREFIX + f'Study registered: {study}'
        body = f'{user} registered a new study:\n\n{link}'
        to = settings.RDM_NOTIFICATIONS_TO

        return send_mail(subject, body, None, to)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['initial'] = {
            'contact_person': self.request.user,
            'department': self.request.user.default_department,
            'owners': [self.request.user],
        }
        return kwargs

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.get_form()
        formset = self.get_formset()
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form, formset)
        else:
            return self.form_invalid(form, formset)

    def form_valid(self, form, formset):
        response = super().form_valid(form, formset)
        form.instance.owners.add(self.request.user)
        self.send_notification()
        return response

    def get_success_url(self):
        return reverse(
            'studies:storage', args=[self.object.pk, self.object.short_title]
        )


class StudyUpdateView(OwnerRequiredMixin, LinkFormsetMixin, UpdateView):
    model = Study
    form_class = StudyUpdateForm
    slug_field = 'short_title'
    query_pk_and_slug = True

    def send_notification(self, old):
        study = str(self.object)
        user = self.request.user.get_full_name()

        subject = settings.EMAIL_SUBJECT_PREFIX + f'Study end changed: {study}'
        body = (
            f'{user} changed the end date of study {study}:\n\n'
            f'old: {old.end}\n'
            f'new: {self.object.end}\n'
        )
        to = settings.RDM_NOTIFICATIONS_TO

        return send_mail(subject, body, None, to)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        formset = self.get_formset()
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form, formset)
        else:
            return self.form_invalid(form, formset)

    def form_valid(self, form, formset):
        old = self.get_object()
        if (
            form.cleaned_data['contact_person'] != old.contact_person
            or set(form.cleaned_data['owners']) != set(old.owners.all())
        ):
            form.instance.ldap_group_synced = False
        if form.cleaned_data['end'] != old.end:
            self.send_notification(old)

        return super().form_valid(form, formset)

    def get_success_url(self):
        return reverse('studies:detail', args=[self.object.pk, self.object.short_title])


class StudyDeleteView(OwnerRequiredMixin, DeleteView):
    model = Study
    slug_field = 'short_title'
    query_pk_and_slug = True

    def send_notification(self):
        study = str(self.object)
        user = self.request.user.get_full_name()
        contact = self.object.contact_person
        storage_location = self.object.storage_location

        subject = settings.EMAIL_SUBJECT_PREFIX + f'Study deleted: {study}'
        body = (
            f'{user} deleted the study {study}:\n\n'
            f'contact person: {contact.get_full_name()} <{contact.email}>\n'
            f'storage location: {storage_location}\n\n'
            'Please check whether the storage folder should be deleted, too!'
        )
        to = settings.RDM_NOTIFICATIONS_TO

        return send_mail(subject, body, None, to)

    def get_success_url(self):
        return reverse('studies:list')

    def form_valid(self, form):
        self.send_notification()
        return super().form_valid(form)


class StorageCreateView(OwnerRequiredMixin, DetailView):
    model = Study
    slug_field = 'short_title'
    query_pk_and_slug = True
    template_name = 'studies/storage_create.html'

    def send_notification(self, study):
        user = self.request.user.get_full_name()
        link = self.request.build_absolute_uri(study.get_absolute_url())
        url = study.storage_location
        unc = smb.unc(url)

        body = (
            f'{user} created a folder for your study "{study}" ({link}).\n\n'
            'The folder is now available at:\n'
            f'{url}\n'
            f'{unc}\n\n'
            'Storage space for the study folder is initially limited to 100 GB, '
            'but you can request more storage space if necessary. '
            'Except for the "private" folder, all subfolders are readable '
            '(but not writable) institute-wide for all researchers. '
            'You can control which users have full access to the "private" folder '
            'via the study registration tool. '
            'Users may need to restart their computer to get full access.\n\n'
            'Please make sure to place all relevant information into that '
            'folder by the end of the study at the latest.\n\n'
            'For support on data organization and documentation, you may want '
            'to visit our intranet pages at '
            'https://max.mpg.de/sites/bild/Service-Bereiche/Bibliothek/fdm '
            'or get in touch at rdm@mpib-berlin.mpg.de'
        )

        return send_study_mail('Study folder created', body, study, self.request)

    def post(self, request, **kwargs):
        study = self.get_object()

        if (
            study.storage_location
            or study.storage_is_archived
            or study.storage_is_deleted
        ):
            raise SuspiciousOperation

        try:
            storage.sync_group(study)
            url = storage.create_folder(study)
        except Exception:
            request_logger.exception('Storage sync failed', extra={'request': request})
            url = None
        if url:
            study.storage_location = url
            study.ldap_group_managed = True
            study.save()

            self.send_notification(study)

            display = smb.url_or_unc(self.request, url)
            messages.success(
                self.request,
                _(
                    'A study folder has been created at {}. '
                    'You may have to restart your computer to get full access.'
                ).format(display),
            )
        else:
            messages.error(
                self.request,
                _('Error while creating the study folder. Please try again later.'),
            )
        return redirect(study.get_absolute_url())


class EthicsConfirmView(LoginRequiredMixin, View):
    def get_mail_body(self, request, study):
        body = (
            'Thank you for storing the ethics application and approval for your '
            f'study {study}. '
            'The RDM team has confirmed that the documents match the registered study. '
            'You are now eligible to proceed with your study'
        )

        next_steps = [
            (name, url_fn(request, study))
            for name, url_fn, departments in settings.ETHICS_NEXT_STEPS
            if not departments or study.department.pk in departments
        ]

        if len(next_steps) > 1:
            body += ', e.g.:\n\n'
            for name, url in next_steps:
                body += f'- {name} ({url})\n\n'
        elif len(next_steps) == 1:
            name, url = next_steps[0]
            body += f', e.g. {name} ({url})\n\n'
        else:
            body += '.\n\n'

        body += (
            'More information on various aspects of the study organization can '
            'be found in MAX: '
            'https://max.mpg.de/sites/bild/Campus/Seiten/Study-Organization.aspx'
        )

        return body

    def post(self, request, **kwargs):
        if not request.user.is_superuser:
            return self.handle_no_permission()
        study = get_object_or_404(Study, pk=kwargs['pk'], short_title=kwargs['slug'])
        study.ethics_confirmed = datetime.date.today()
        study.save()

        body = self.get_mail_body(request, study)
        send_study_mail('Ethics approval confirmed', body, study, request)

        messages.success(self.request, _('Ethics approval has been confirmed.'))
        return redirect(study.get_absolute_url())


class StaffDataUpdateView(StaffRequiredMixin, UpdateView):
    model = Study
    fields = ['staff_status', 'staff_followup']
    slug_field = 'short_title'
    query_pk_and_slug = True
    template_name = 'studies/staff_data_form.html'

    def get_success_url(self):
        return reverse('studies:detail', args=[self.object.pk, self.object.short_title])


class CalendarView(StaffRequiredMixin, TemplateView):
    template_name = 'studies/calendar.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        today = datetime.date.today()
        year = self.kwargs.get('year', today.year)
        month = self.kwargs.get('month', today.month)

        items = []

        for study in Study.objects.filter(
            created_at__year=year, created_at__month=month
        ):
            items.append((study.created_at, study, _('registration'), 'primary'))

        for study in Study.objects.filter(end__year=year, end__month=month):
            items.append((study.end, study, _('end'), 'danger'))

        for study in Study.objects.annotate(
            expected_end=models.ExpressionWrapper(
                models.F('created_at') + datetime.timedelta(days=365),
                output_field=models.DateField(),
            ),
        ).filter(end=None, expected_end__year=year, expected_end__month=month):
            # depending on database backend, this is either a date or a datetime
            expected_end = ensure_date(study.expected_end)
            items.append(
                (expected_end, study, _('1 year after registration'), 'danger')
            )

        for study in Study.objects.filter(
            staff_followup__year=year, staff_followup__month=month
        ):
            if study.staff_status:
                items.append((
                    study.staff_followup,
                    study,
                    study.staff_status,
                    study.staff_status.color,
                ))
            else:
                items.append((
                    study.staff_followup,
                    study,
                    _('follow-up'),
                    'secondary',
                ))

        for d in DataSet.objects.filter(
            collection_start__year=year,
            collection_start__month=month,
        ).select_related('study'):
            items.append(
                (d.collection_start, d.study, _('collection start'), 'success')
            )

        for d in DataSet.objects.filter(
            collection_end__year=year,
            collection_end__month=month,
        ).select_related('study'):
            items.append((d.collection_end, d.study, _('collection end'), 'warning'))

        context['year'] = year
        context['month'] = month
        context['items'] = sorted(items, key=lambda x: x[0])

        return context


class StudyMaintenanceView(StaffRequiredMixin, ListView):
    template_name = 'studies/study_maintenance.html'
    model = Study
    paginate_by = 20

    def get_queryset(self):
        _filter = self.request.GET.get('filter', 'end')
        qs = (
            super().get_queryset()
            .select_related('contact_person')
            .prefetch_related('keyword_set')
            .order_by('created_at')
        )

        if _filter == 'archive':
            today = datetime.date.today()
            return (
                qs.filter(end__lt=today, storage_archived=None, storage_deleted=None)
                .exclude(storage_location=None)
            )
        elif _filter == 'inactive':
            return qs.filter(contact_person__is_active=False)
        else:
            return qs.filter(end=None)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filters'] = [
            ('end', _('No end date')),
            ('archive', _('Can be archived')),
            ('inactive', _('Inactive contact')),
        ]
        return context


class StudyArchiveTxtView(LoginRequiredMixin, DetailView):
    model = Study
    slug_field = 'short_title'
    query_pk_and_slug = True
    content_type = 'txt/plain'
    template_name = 'studies/archive.txt'

    def get(self, request, **kwargs):
        response = super().get(request, **kwargs)
        response['Content-Disposition'] = (
            f'attachment; filename="archive-{self.object}.txt"'
        )
        return response
