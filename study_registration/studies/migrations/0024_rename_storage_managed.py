from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("studies", "0023_study_view_storage_permission"),
    ]

    operations = [
        migrations.AlterField(
            model_name="study",
            name="storage_managed",
            field=models.BooleanField(
                default=False,
                help_text="If selected, access permissions for the storage location will automatically be updated when owners change.",
                verbose_name="LDAP group is managed",
            ),
        ),
        migrations.RenameField(
            model_name="study",
            old_name="storage_managed",
            new_name="ldap_group_managed",
        ),
    ]
