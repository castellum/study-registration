from django import forms
from django.db import models
from django.utils.translation import gettext_lazy as _

from study_registration.utils.forms import TagField

from .models import Department
from .models import Keyword
from .models import Study


class StudyUpdateForm(forms.ModelForm):
    keywords = TagField(
        label=_('Keywords'),
        help_text=_(
            "Describe your study's research topic, the data type "
            'acquired, methods used, etc. These keywords allow '
            'other researchers to find your study and get in '
            'touch if they are working on related topics. You can '
            'select keywords that have been used before from the auto-'
            'completion. If none of the existing keywords sound right, '
            'you can also create a new keyword by hitting enter. '
            'Please carefully check for existing synonyms before '
            'creating a new keyword.'
        ),
        initial=[],
        required=False,
    )

    class Meta:
        model = Study
        fields = [
            'title',
            'aliases',
            'end',
            'contact_person',
            'department',
            'project',
            'affiliated_scientists',
            'owners',
            'ethics_board',
            'description',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for key in ['contact_person', 'owners']:
            self.fields[key].empty_label = ''
            self.fields[key].widget.attrs.update({
                'data-select': True,
                'data-select-input-class': 'form-control',
                'data-select-value-class': 'badge text-bg-secondary badge-delete',
            })

        self.fields['department'].queryset = Department.objects.filter(
            models.Q(is_active=True) | models.Q(pk=self.instance.department_id)
        )

        self.fields['keywords'].choices = [
            (s, s) for s in (
                Keyword.objects
                .values_list('name', flat=True)
                .alias(count=models.Count('name'))
                .order_by('-count')
                .distinct()
            )
        ]

        if self.instance.pk:
            self.fields['keywords'].initial = list(
                self.instance.keyword_set.values_list('name', flat=True)
            )

    def save(self, *args, **kwargs):
        obj = super().save(*args, **kwargs)

        for keyword in obj.keyword_set.all():
            if keyword.name not in self.cleaned_data['keywords']:
                keyword.delete()
        for name in self.cleaned_data['keywords']:
            obj.keyword_set.get_or_create(name=name)

        return obj


class StudyCreateForm(StudyUpdateForm):
    class Meta:
        model = Study
        fields = [
            *StudyUpdateForm.Meta.fields,
            'short_title',
        ]
        labels = {
            'short_title': _('Short title (cannot be changed later)')
        }
