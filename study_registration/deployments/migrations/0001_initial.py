from django.db import migrations, models
import django.db.models.deletion
import study_registration.utils.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('studies', '0006_alter_study_end'),
    ]

    operations = [
        migrations.CreateModel(
            name='Deployment',
            fields=[
                (
                    'id',
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                (
                    'created_at',
                    models.DateField(auto_now_add=True, verbose_name='Created at'),
                ),
                (
                    'git_ref',
                    models.CharField(
                        default='main', max_length=64, verbose_name='Branch'
                    ),
                ),
                (
                    'git_file',
                    models.CharField(
                        blank=True,
                        help_text='A desktop shortcut to this file will be created on the laboratory computers.',
                        max_length=256,
                        verbose_name='Name of the file within the repository that starts your experiment',
                    ),
                ),
                (
                    'commit_sha',
                    models.CharField(max_length=50, verbose_name='Commit (version)'),
                ),
                (
                    'startdate',
                    study_registration.utils.fields.DateField(
                        verbose_name='Start of deployment'
                    ),
                ),
                (
                    'enddate',
                    study_registration.utils.fields.DateField(
                        help_text='Your files and collected data will automatically be deleted from the deployment target after that date.',
                        verbose_name='End of deployment',
                    ),
                ),
                (
                    'git_repo',
                    models.CharField(max_length=256, verbose_name='Repository URL'),
                ),
                (
                    'gitlab_id',
                    models.IntegerField(
                        help_text='The project ID is displayed right below the project title on GitLab.',
                        verbose_name='GitLab project ID',
                    ),
                ),
                (
                    'targeturl',
                    models.CharField(
                        blank=True, max_length=300, verbose_name='Target URL'
                    ),
                ),
                (
                    'status',
                    models.IntegerField(
                        choices=[(0, 'Not running'), (10, 'Running'), (20, 'Finished')],
                        default=0,
                        verbose_name='Status',
                    ),
                ),
                (
                    'study',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to='studies.study',
                        verbose_name='Study',
                    ),
                ),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='DeployTarget',
            fields=[
                (
                    'id',
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                (
                    'name',
                    models.CharField(max_length=256, unique=True, verbose_name='Name'),
                ),
                (
                    'key',
                    models.SlugField(max_length=32, unique=True, verbose_name='key'),
                ),
                (
                    'gitlab_deploy_key',
                    models.PositiveIntegerField(
                        blank=True, null=True, verbose_name='GitLab deploy key'
                    ),
                ),
                (
                    'url_template',
                    models.CharField(
                        blank=True, max_length=256, verbose_name='Target URL template'
                    ),
                ),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='DeploymentRevision',
            fields=[
                (
                    'id',
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                (
                    'created_at',
                    models.DateField(auto_now_add=True, verbose_name='Created at'),
                ),
                (
                    'git_ref',
                    models.CharField(
                        default='main', max_length=64, verbose_name='Branch'
                    ),
                ),
                (
                    'git_file',
                    models.CharField(
                        blank=True,
                        help_text='A desktop shortcut to this file will be created on the laboratory computers.',
                        max_length=256,
                        verbose_name='Name of the file within the repository that starts your experiment',
                    ),
                ),
                (
                    'commit_sha',
                    models.CharField(max_length=50, verbose_name='Commit (version)'),
                ),
                (
                    'startdate',
                    study_registration.utils.fields.DateField(
                        verbose_name='Start of deployment'
                    ),
                ),
                (
                    'enddate',
                    study_registration.utils.fields.DateField(
                        help_text='Your files and collected data will automatically be deleted from the deployment target after that date.',
                        verbose_name='End of deployment',
                    ),
                ),
                (
                    'deployment',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name='revisions',
                        to='deployments.deployment',
                    ),
                ),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='deployment',
            name='target',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                to='deployments.deploytarget',
                verbose_name='Deploy target',
            ),
        ),
    ]
