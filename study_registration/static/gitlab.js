import makeSpinner from './spinner.js'

var form = document.querySelector('[data-gitlab-project]');
var idField = document.getElementById('id_gitlab_id');
var repoField = document.getElementById('id_git_repo');
var branchField = document.getElementById('id_git_ref');
var commitField = document.getElementById('id_commit_sha');
var fileField = document.getElementById('id_git_file');

var projectUrl = form.dataset.gitlabProject;
var commitsUrl = form.dataset.gitlabCommits;
var filesUrl = form.dataset.gitlabFiles;

var idError = document.createElement('div');
idError.textContent = 'This project ID could not be found.';
idError.className = 'form-text text-danger';
idError.hidden = true;
idField.parentElement.append(idError);

var toggleSpinner = makeSpinner(idField);

var _fetch = function(path, params) {
    var url = path + '?' + new URLSearchParams(params);
    toggleSpinner(true);
    return fetch(url, {credentials: 'same-origin'}).then(res => {
        if (res.ok) {
            return res.json();
        } else {
            throw res;
        }
    }).finally(() => toggleSpinner(false));
};

var replaceBySelect = function(field) {
    var select = document.createElement('select');
    select.className = 'form-select';
    select.name = field.name;
    select.request = field.required;
    field.before(select);
    field.remove();
    select.id = field.id;
    return select;
};

var addDataList = function(field) {
    var list = document.createElement('datalist');
    list.id = field.id + '_list';
    field.after(list);
    field.setAttribute('list', list.id);
    return list;
};

var makeOptions = function(select, options) {
    select.innerHTML = '';
    options.forEach(data => {
        var option = document.createElement('option');
        option.value = data[0];
        option.textContent = data[1];
        select.append(option);
    });
};

var branchSelect = replaceBySelect(branchField);
var commitSelect = replaceBySelect(commitField);
var fileList = addDataList(fileField);

repoField.readOnly = true;
branchSelect.disabled = true;
commitSelect.disabled = true;
fileField.disabled = true;

var onId = function(keepValue) {
    branchSelect.disabled = true;
    commitSelect.disabled = true;
    fileField.disabled = true;

    if (!idField.value) {
        return Promise.reject();
    }

    return _fetch(projectUrl, {
        'gitlab_id': idField.value,
    }).catch(err => {
        idError.hidden = false;
        throw err;
    }).then(data => {
        idError.hidden = true;
        repoField.value = data.git_repo;
        makeOptions(branchSelect, data.branches.map(b => [b, b]));
        branchSelect.value = (keepValue && branchField.value) || data.default_branch;
        branchSelect.disabled = false;
        return onBranch(keepValue);
    });
};

var onBranch = function(keepValue) {
    commitSelect.disabled = true;
    fileField.disabled = true;

    if (!idField.value || !branchSelect.value) {
        return Promise.reject();
    }

    return _fetch(commitsUrl, {
        'gitlab_id': idField.value,
        'branch': branchSelect.value,
    }).then(data => {
        makeOptions(commitSelect, data.commits.map(c => [
            c.id,
            `${c.id.substr(0, 8)} - ${c.title}`
        ]));
        if (keepValue) {
            commitSelect.value = commitField.value;
        }
        commitSelect.disabled = false;
        return onCommit(keepValue);
    });
};

var onCommit = function(keepValue) {
    fileField.disabled = true;

    if (!idField.value || !commitSelect.value) {
        return Promise.reject();
    }

    return _fetch(filesUrl, {
        'gitlab_id': idField.value,
        'ref': commitSelect.value,
    }).then(data => {
        fileField.disabled = false;
        makeOptions(fileList, data.files.map(f => [f, f]));
    });
};

fileField.autocomplete = 'off';

idField.addEventListener('blur', () => idField.reportValidity());
idField.addEventListener('change', () => onId(false));
branchSelect.addEventListener('change', () => onBranch(false));
commitSelect.addEventListener('change', () => onCommit(false));

onId(true);
