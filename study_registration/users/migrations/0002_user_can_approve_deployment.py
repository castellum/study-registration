from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial2'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='can_approve_deployment',
            field=models.BooleanField(
                default=False, verbose_name='Can approve deployment'
            ),
        ),
    ]
