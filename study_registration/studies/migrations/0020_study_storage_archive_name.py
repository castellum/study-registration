from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('studies', '0019_department_is_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='study',
            name='storage_archive_name',
            field=models.CharField(blank=True, max_length=255, verbose_name='Storage archive name'),
        ),
    ]
