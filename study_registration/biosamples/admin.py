from django.contrib import admin

from .models import BioSampleSet

admin.site.register(BioSampleSet)
