from django.conf import settings
from django.urls import reverse
from django.views.generic import CreateView
from django.views.generic import DeleteView
from django.views.generic import UpdateView

from study_registration.studies.mixins import StudyAspectMixin
from study_registration.utils.forms import DatalistWidget
from study_registration.utils.mail import send_mail

from .models import BioSampleSet


class BioSampleSetFormMixin(StudyAspectMixin):
    model = BioSampleSet
    fields = [
        'sample_type',
        'count',
        'temperature',
        'processing',
        'analyses',
        'laboratories',
        'keep_analyzed',
        'keep_backup',
        'date_of_disposal',
        'notes',
    ]

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields['sample_type'].widget = DatalistWidget(datalist=(
            BioSampleSet.objects
            .values_list('sample_type', flat=True)
            .distinct()
        ))
        form.fields['processing'].widget.attrs['rows'] = 2
        form.fields['analyses'].widget.attrs['rows'] = 2
        form.fields['laboratories'].widget.attrs['rows'] = 2
        form.fields['notes'].widget.attrs['rows'] = 3
        return form


class BioSampleSetCreateView(BioSampleSetFormMixin, CreateView):
    def send_notification(self):
        to = settings.BIOSAMPLES_NOTIFICATIONS_TO
        subject = settings.EMAIL_SUBJECT_PREFIX + 'Bio samples registered'

        obj = self.object
        user = self.request.user.get_full_name()
        study = self.study.title
        link = self.request.build_absolute_uri(
            reverse('studies:detail', args=[
                self.study.pk, self.study.short_title
            ])
        )
        body = (
            f'{user} registered bio samples "{obj}" ({obj.count}) '
            f'for study "{study}".\n\n'
            f'View details at {link}'
        )

        return send_mail(subject, body, None, to)

    def form_valid(self, form):
        form.instance.study = self.study
        response = super().form_valid(form)
        self.send_notification()
        return response


class BioSampleSetUpdateView(BioSampleSetFormMixin, UpdateView):
    pass


class BioSampleSetDeleteView(StudyAspectMixin, DeleteView):
    model = BioSampleSet
