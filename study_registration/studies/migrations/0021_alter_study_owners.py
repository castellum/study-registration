from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("studies", "0020_study_storage_archive_name"),
    ]

    operations = [
        migrations.AlterField(
            model_name="study",
            name="owners",
            field=models.ManyToManyField(
                limit_choices_to={"is_active": True},
                to=settings.AUTH_USER_MODEL,
                verbose_name="Users who can edit this study profile and access the private folder",
            ),
        ),
    ]
