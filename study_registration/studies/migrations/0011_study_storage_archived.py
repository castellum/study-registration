from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('studies', '0010_storage'),
    ]

    operations = [
        migrations.AddField(
            model_name='study',
            name='storage_archived',
            field=models.DateField(
                blank=True, null=True, verbose_name='Storage archived'
            ),
        ),
    ]
