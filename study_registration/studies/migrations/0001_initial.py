from django.core import validators
from django.db import migrations, models
import study_registration.utils.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name='Department',
            fields=[
                (
                    'id',
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                (
                    'name',
                    models.CharField(max_length=254, unique=True, verbose_name='Name'),
                ),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Keyword',
            fields=[
                (
                    'id',
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                ('name', models.CharField(max_length=128, verbose_name='Name')),
            ],
        ),
        migrations.CreateModel(
            name='Study',
            fields=[
                (
                    'id',
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                (
                    'created_at',
                    models.DateField(auto_now_add=True, verbose_name='Created at'),
                ),
                (
                    'updated_at',
                    models.DateField(auto_now=True, verbose_name='Updated at'),
                ),
                ('title', models.CharField(max_length=254, verbose_name='Title')),
                (
                    'short_title',
                    models.CharField(
                        help_text='The short title will be used to refer to this study within the institute context, e.g., to create a physical folder on the server. Therefore, only use lower-case letters, numbers, or hyphens.',
                        max_length=254,
                        validators=[
                            validators.RegexValidator('^[-a-z0-9]+\\Z', 'Only use lower-case letters, numbers, and hyphens.'),
                            validators.MaxLengthValidator(16),
                        ],
                        verbose_name='Short title',
                    ),
                ),
                (
                    'aliases',
                    models.CharField(
                        blank=True,
                        help_text='Please compile a complete list of additional titles that may refer to your study, including e.g. a German title used in communication with participants.',
                        max_length=254,
                        verbose_name='Other titles or acronyms',
                    ),
                ),
                (
                    'end',
                    study_registration.utils.fields.DateField(
                        blank=True,
                        help_text='This refers to the timepoint when analyses are completed, potential manuscripts are submitted and the data will most likely no longer be actively processed.',
                        null=True,
                        verbose_name='Expected end of study',
                    ),
                ),
                (
                    'description',
                    models.TextField(
                        blank=True,
                        help_text='This text should allow other researchers to quickly grasp whether this study and the related data could be of interest to them.',
                        verbose_name='Description',
                    ),
                ),
                (
                    'url',
                    models.URLField(
                        blank=True,
                        help_text='Any URL related to this study, e.g. project website, OSF repo.',
                        verbose_name='Website',
                    ),
                ),
                (
                    'affiliated_scientists',
                    models.CharField(
                        help_text='You can use the same format as you would use for the authors of a paper.',
                        max_length=254,
                        verbose_name='Affiliated scientists',
                    ),
                ),
            ],
            options={
                'ordering': ['-pk'],
            },
        ),
    ]
