var approvalBlock = document.querySelector('[data-threshold]');
var expectedField = document.querySelector('[name="expected"]');

var update = function() {
    var threshold = parseFloat(approvalBlock.dataset.threshold, 10);
    var value = parseFloat(expectedField.value, 10);
    var required = value >= threshold;

    approvalBlock.hidden = !required;
    approvalBlock.querySelectorAll('input,textarea').forEach(input => {
        input.required = required;
        input.closest('.mb-3').classList.toggle('form-required', required);
    });
};

expectedField.addEventListener('input', update);
update();
