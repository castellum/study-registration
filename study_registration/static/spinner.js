export default function makeSpinner(field) {
    var count = 0;

    var group = document.createElement('div');
    group.className = 'position-relative';
    field.before(group);
    group.append(field);

    var spinner = document.createElement('div');
    spinner.hidden = true;
    group.append(spinner);

    var wrapper = document.createElement('div');
    wrapper.className = 'position-absolute end-0 top-0 h-100 d-flex align-items-center me-3';
    spinner.append(wrapper);

    var inner = document.createElement('div');
    inner.className = 'spinner-border spinner-border-sm';
    inner.setAttribute('role', 'status');
    inner.setAttribute('aria-label', 'Loading…');
    wrapper.append(inner);

    return function(show) {
        count += show ? 1 : -1;
        spinner.hidden = count === 0;
        field.classList.toggle('pe-5', !spinner.hidden);
    };
}
