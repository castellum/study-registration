from django.db import migrations, models


def migrate_data(apps, schema_editor):
    Deployment = apps.get_model('deployments', 'Deployment')

    for deployment in Deployment.objects.all().select_related('target'):
        deployment.legacy_branch = f'asr_deploy_to_{deployment.target.key}'
        deployment.save()


class Migration(migrations.Migration):

    dependencies = [
        ("deployments", "0005_deployment_deployed"),
    ]

    operations = [
        migrations.AddField(
            model_name="deployment",
            name="legacy_branch",
            field=models.CharField(
                blank=True, max_length=254, verbose_name="Legacy branch"
            ),
        ),
        migrations.RunPython(migrate_data),
    ]
