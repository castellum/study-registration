from django.views.generic import CreateView
from django.views.generic import DeleteView
from django.views.generic import UpdateView

from study_registration.studies.mixins import StudyAspectMixin

from .forms import DataSetForm
from .models import DataSet


class DataSetFormMixin(StudyAspectMixin):
    model = DataSet

    def get_form(self):
        form = DataSetForm(**self.get_form_kwargs())
        reuse_study = form.fields['reuse_study']
        reuse_study.queryset = reuse_study.queryset.exclude(pk=self.study.pk)
        return form


class DataSetCreateView(DataSetFormMixin, CreateView):
    def form_valid(self, form):
        form.instance.study = self.study
        return super().form_valid(form)


class DataSetUpdateView(DataSetFormMixin, UpdateView):
    pass


class DataSetDeleteView(StudyAspectMixin, DeleteView):
    model = DataSet
