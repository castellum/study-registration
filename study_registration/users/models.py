from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import UserManager as _UserManager
from django.db import models
from django.utils.translation import gettext_lazy as _

from study_registration.studies.models import Department


class UserManager(_UserManager):
    def with_perm(self, perm, *args, **kwargs):
        backend = 'django.contrib.auth.backends.ModelBackend'
        return super().with_perm(perm, *args, backend=backend, **kwargs)


class User(AbstractUser):
    default_department = models.ForeignKey(
        Department,
        verbose_name=_('Default department'),
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    phone = models.CharField(max_length=64, blank=True)
    office = models.CharField(max_length=64, blank=True)

    objects = UserManager()

    def get_full_name(self):
        return super().get_full_name() or self.get_username()

    def __str__(self):
        if self.email:
            return f'{self.get_full_name()} <{self.email}>'
        else:
            return self.get_full_name()
