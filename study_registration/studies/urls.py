from django.urls import path

from .api import StudyJSONView
from .api import StudyMarcXMLView
from .views import CalendarView
from .views import EthicsConfirmView
from .views import MyStudiesView
from .views import StaffDataUpdateView
from .views import StorageCreateView
from .views import StudyArchiveTxtView
from .views import StudyCreateView
from .views import StudyDeleteView
from .views import StudyDetailView
from .views import StudyListView
from .views import StudyMaintenanceView
from .views import StudyRedirectView
from .views import StudyUpdateView

app_name = 'studies'
urlpatterns = [
    path('', StudyListView.as_view(), name='list'),
    path('mine/', MyStudiesView.as_view(), name='mine'),
    path('new/', StudyCreateView.as_view(), name='create'),
    path('calendar/', CalendarView.as_view(), name='calendar'),
    path('calendar/<int:year>-<int:month>/', CalendarView.as_view(), name='calendar'),
    path('<int:pk>/', StudyRedirectView.as_view()),
    path('<int:pk>-<slug:slug>/', StudyDetailView.as_view(), name='detail'),
    path('<int:pk>-<slug:slug>.json', StudyJSONView.as_view(), name='api'),
    path('<int:pk>-<slug:slug>.mrcx', StudyMarcXMLView.as_view(), name='marc-xml'),
    path('<int:pk>-<slug:slug>/update/', StudyUpdateView.as_view(), name='update'),
    path('<int:pk>-<slug:slug>/delete/', StudyDeleteView.as_view(), name='delete'),
    path('<int:pk>-<slug:slug>/storage/', StorageCreateView.as_view(), name='storage'),
    path('<int:pk>-<slug:slug>/archive.txt', StudyArchiveTxtView.as_view()),
    path('<int:pk>-<slug:slug>/ethics/', EthicsConfirmView.as_view(), name='ethics'),
    path('<int:pk>-<slug:slug>/staff/', StaffDataUpdateView.as_view(), name='staff'),
    path('maintenance/', StudyMaintenanceView.as_view(), name='maintenance'),
]
