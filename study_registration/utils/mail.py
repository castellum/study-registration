from django.conf import settings
from django.core.mail import EmailMessage


def send_mail(subject, body, from_email, to):
    msg = EmailMessage(
        subject=subject,
        body=body,
        from_email=from_email,
        to=to,
        headers={'Auto-Submitted': 'auto-generated'},
    )
    return msg.send()


def send_study_mail(subject, body, study, request, additional_to=[]):
    if isinstance(request, str):
        link = request + study.get_absolute_url()
    else:
        link = request.build_absolute_uri(study.get_absolute_url())

    full_subject = settings.EMAIL_SUBJECT_PREFIX + f'[{study}] {subject}'
    full_body = (
        f'{body}\n\n'
        'Thanks for keeping your registered study up-to-date!\n\n'
        '-- \n'
        'You are receiving this mail because you are listed as a '
        f'study member at {link}.'
    )
    to = (
        study.owners
        .exclude(email='')
        .exclude(is_active=False)
        .values_list('email', flat=True)
    )

    return send_mail(full_subject, full_body, None, [*to, *additional_to])
